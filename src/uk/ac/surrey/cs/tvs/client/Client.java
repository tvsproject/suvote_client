/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyManagementException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.security.auth.x500.X500Principal;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v1CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v1CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.BallotGenMix;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.AuditFileCreationException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenAuditException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenerationException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.FileBackedTimeoutManager;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDef;
import uk.ac.surrey.cs.tvs.utils.conf.RaceDefs;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.KeyGeneration;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.GenerateSignatureKeyAndCSR;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;
import uk.ac.surrey.cs.tvs.utils.exceptions.MaxTimeoutExceeded;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Provides a single client object for calling the various functions of the client. These include Key Generation, CSR generation,
 * Certificate importing and the full ballot generation and audit process.
 * 
 * Any UI should create an instance of this class to call the various methods on. The processes involved need to be called in a
 * particular order. For example, before a certificate can be imported a Certificate Signing Request should have been generated.
 * This class will eventually enforce the order of calling for these methods.
 * 
 * 
 * @author Chris Culnane
 * 
 */
public class Client {

  /**
   * String array of field values that appear in the config file as a result of a process being run. Should be in the same order as
   * the processes should be called
   */
  private String[]                 statusParams;

  /**
   * Constant String array of the init steps for a VPS machine
   */
  private static final String[]    VPS_PARAMS          = new String[] { "SigningKeyStore", "SSLCSRLocation", "CertificateLocation",
      "EncryptionKeyStore", "BallotsGenerated", "BallotsAudited" };

  /**
   * Constant String array of the init steps for a EVM machine
   */
  private static final String[]    EVM_PARAMS          = new String[] { "SigningKeyStore", "SSLCSRLocation", "CertificateLocation" };

  /**
   * Constant String array of the init steps for a Cancel machine
   */
  private static final String[]    CANCEL_PARAMS       = new String[] { "SigningKeyStore", "SSLCSRLocation", "CertificateLocation" };

  /**
   * Constant String array of the init steps for a EVM machine
   */
  private static final String[]    FILE_PARAMS         = new String[] { "SigningKeyStore", "SSLCSRLocation", "CertificateLocation" };

  /**
   * Constant String to indicate the client is initialised
   */
  public static final String       INITIALISED_PARAM   = "Initialised";

  /**
   * Constants holding the name of the TimeoutThread
   */
  private static final String      TIMEOUT_THREAD_NAME = "ClientTimeoutManagerThread";
  /**
   * TimeoutManager to handle the automatic deletion of used ballot files
   */
  private FileBackedTimeoutManager timeoutManager;

  /**
   * Challenge password for CSR - we leave this blank because certificate revocation will be centrally controlled task, not
   * something individual system entities can request
   */
  private static final String      CHALLENGE_PASSWORD  = "";
  /**
   * ConfigFile for this client that specifies paths to various files generated
   */
  private ConfigFile               conf;

  /**
   * Logger
   */
  protected static final Logger    logger              = LoggerFactory.getLogger(Client.class);

  /**
   * Stores the current status level
   */
  private int                      statusLevel         = -1;

  /**
   * Stores whether this client has finished all the initialisation steps and is ready to serve content
   */
  private boolean                  finishedInit        = false;

  /**
   * Current progress
   */
  private float                    progress            = 0f;

  /**
   * Last integer progress amount, used to increment an integer progress listener with a float based progress value
   */
  private int                      lastProgress        = 0;
  /**
   * List of ProgressListeners
   */
  private List<ProgressListener>   progressListeners   = new ArrayList<ProgressListener>();

  /**
   * SSLSocketFactory to use for creating sockets to communicate with the MBB
   */
  private SSLSocketFactory         factory;

  /**
   * String array of acceptable cipher suites
   */
  private String[]                 cipherSuites        = null;

  /**
   * Thread to run TimeoutManager in
   */
  private Thread                   timeoutManagerThread;

  /**
   * ClientType that represents whether this is a VPS,EVM,CANCEL client
   */
  private ClientType               type;

  /**
   * Schema used for config file.
   */
  private static final String      CONFIG_SCHEMA       = "configschema.json";

  /**
   * Base path to config files
   */
  protected static String          BASE_PATH;
  
  /**
   * Boolean to record if the client has started
   */
  private boolean                  hasStarted          = false;

  /**
   * RaceDefs containing race definitions
   */
  private RaceDefs                 raceDefs            = new RaceDefs();

  /**
   * Set the initialisation path. We do this because when created on the client (Android) we cannot get the current directory and
   * hence have to hardcode a path
   */
  protected void initPaths() {
    BASE_PATH = "/sdcard/vvoteclient/";
  }

  /**
   * Creates an instance of client using the ConfigFile located at the path specified
   * 
   * @param configFile
   *          string path to ConfigFile
   * @throws JSONIOException
   * @throws IOException
   * @throws PeerSSLInitException
   * @throws MaxTimeoutExceeded
   */
  public Client(String configFile) throws JSONIOException, IOException, PeerSSLInitException, MaxTimeoutExceeded {
    super();
    initPaths();
    logger.info("Created Client object with Config:{}", configFile);

    this.conf = new ConfigFile(configFile);// ,BASE_PATH+CONFIG_SCHEMA);//, BASE_PATH+CONFIG_SCHEMA);
    this.type = ClientType.valueOf(this.conf.getStringParameter(ClientConfig.PURPOSE));
    logger.info("Client is {}", this.type);

    try {
      JSONArray raceArr = IOUtils.readJSONArrayFromFile(this.conf.getStringParameter(ClientConfig.RACES_CONF));
      this.raceDefs = new RaceDefs();
      for (int i = 0; i < raceArr.length(); i++) {
        RaceDef raceDef = new RaceDef(raceArr.getJSONObject(i));
        raceDefs.addRaceDef(raceDef);
      }
    }
    catch (JSONException e) {
      throw new JSONIOException("Exception reading races config", e);
    }

    switch (this.type) {
      case CANCEL:
        statusParams = CANCEL_PARAMS;
        break;
      case EVM:
        statusParams = EVM_PARAMS;
        break;
      case VPS:
        statusParams = VPS_PARAMS;
        logger.info("Creating TimeoutManager and starting thread");
        // If this is a VPS device we need a timeout manager to auto-delete ballots after a preset period of time
        this.timeoutManager = new FileBackedTimeoutManager(this.conf.getIntParameter(ClientConfig.BALLOT_TIMEOUT_SEGMENT),
            this.conf.getIntParameter(ClientConfig.BALLOT_TIMEOUT), this.conf.getIntParameter(ClientConfig.BALLOT_TIMEOUT),
            this.conf.getStringParameter(ClientConfig.TIMEOUT_BACKUP_DIR), new DeleteBallotWorker());
        this.timeoutManagerThread = new Thread(this.timeoutManager, Client.TIMEOUT_THREAD_NAME);
        this.timeoutManagerThread.setDaemon(true);

        break;
      case FILE:
        statusParams = FILE_PARAMS;
        break;
      default:
        throw new JSONIOException("Unknown client purpose");
    }
    this.initComms();
    this.checkStatus();
  }

  /**
   * Starts the timeout manager thread if this is VPS device
   */
  public void start() {
    if (!hasStarted) {
      if (this.type == ClientType.VPS) {
        this.timeoutManagerThread.start();
        logger.info("Have started timeout manager thread");
      }
    }
  }

  /**
   * Adds a ProgressListener to this class
   * 
   * @param listener
   *          ProgressListener to add
   */
  public void addProgressListener(ProgressListener listener) {
    this.progressListeners.add(listener);
  }

  /**
   * Adds a field and value to the distinguished name
   * 
   * @param dn
   *          StringBuffer to add the new field to
   * @param dnKey
   *          String of the key
   * @param value
   *          String of the value
   */
  private void addToDistinguishedName(StringBuffer dn, String dnKey, String value) {
    dn.append(", ");
    dn.append(dnKey);
    dn.append("=");
    dn.append(value);
  }

  /**
   * Checks which of the statusParams exist in the config file. The values in statusParams related to config items that are created
   * as a result of the related method being called. For example, generation of a digital signature key pair will result in a field
   * called SigningKeyStore being created in the config file. If it does not exist it indicates that is the next process to be
   * completed.
   */
  private void checkStatus() {
    int currentLevel = 0;

    // Loop through all the specified field names
    for (String s : this.statusParams) {
      if (this.conf.hasParam(s)) {
        // If the parameter exists we have successfully completed that operation
        currentLevel++;
      }
      else {
        // if doesn't that is the next operation to complete
        break;
      }
    }

    // Set the current status level
    this.statusLevel = currentLevel;

    if (currentLevel == this.statusParams.length) {
      // If we have completed all levels the initialisation is complete
      this.finishedInit = true;
      logger.info("Client is initialised");
    }
    else {
      // otherwise we need to do some more initialisation before this client can serve ballot data
      this.finishedInit = false;
      logger.info("Client requires further initialisation. Next step: {}", this.statusParams[currentLevel]);
    }
  }

  /**
   * Calls the relevant generate and audit methods for the ballot generation. The parameters for ballot generation are obtained from
   * the client configuration file.
   * 
   * @throws ClientException
   */
  public void generateBallots() throws ClientException {
    try {
      // Call the ballot generation
      logger.info("About to generate ballots");

      BallotGenMix ballotGen = new BallotGenMix(this.conf);
      ballotGen.setSSLFactoryAndCipherSuite(this.factory, this.cipherSuites);
      for (ProgressListener pl : this.progressListeners) {
        ballotGen.addProgressListener(pl);
      }

      ballotGen.generateBallots(this.conf.getStringParameter(ConfigFiles.ClientConfig.BALLOT_OUTPUT_FOLDER),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.CIPHERS_FILE));

      logger.info("Finished generating ballots ballots");

      // Audit the ballot generation
      logger.info("About to audit ballot generation");

      JSONObject cipherSubmissionResponse = ballotGen.submitBallotsToWBB(
          this.conf.getStringParameter(ConfigFiles.ClientConfig.CIPHERS_FILE),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.CIPHER_SUBMISSION_FILE));
      this.incrementProgress(10);
      IOUtils.writeJSONToFile(cipherSubmissionResponse,
          this.conf.getStringParameter(ConfigFiles.ClientConfig.CIPHER_SUBMISSION_RESPONSE));
      this.incrementProgress(10);
      this.conf.setStringParameter(ConfigFiles.ClientConfig.BALLOTS_GENERATED, Calendar.getInstance().getTime().toString());

      ballotGen.performAudit(cipherSubmissionResponse, this.conf.getStringParameter(ConfigFiles.ClientConfig.CIPHERS_FILE),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.BALLOTS_TO_AUDIT_FILE),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.BALLOT_GEN_AUDIT_OUTPUT),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.ENCRYPTION_KEY_STORE),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.RANDOMNESS_FOLDER),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.BALLOT_GEN_AUDIT_RESPONSE),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.BALLOT_GEN_AUDIT_SUBMISSION));

      logger.info("Finished ballot generation audit");

      this.conf.setStringParameter(ConfigFiles.ClientConfig.BALLOTS_AUDITED, Calendar.getInstance().getTime().toString());
    }
    catch (IOException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (JSONIOException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (JSONException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (BallotGenerationException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (RandomnessCommitmentException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (CombinedRandomnessException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (AuditFileCreationException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (BallotGenAuditException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (CryptoIOException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
    catch (MBBCommunicationException e) {
      logger.error("Exception during Ballot Generation and Audit", e);
      throw new ClientException("Exception during Ballot Generation and Audit", e);
    }
  }

  /**
   * Generates the KeyPair for crypto operations. This is a per device key that will be used for encrypting session AES Key as part
   * of ballot generation.
   * 
   * @throws ClientException
   */
  public void generateCryptoKeyPair() throws ClientException {
    try {
      logger.info("About to generate Crypto KeyPair");
      this.updateProgressListeners(20);
      String id = this.conf.getStringParameter(ConfigFiles.ClientConfig.ID);
      KeyPair kp = CryptoUtils.generatorElGamalKeyPair(this.conf.getIntParameter(ConfigFiles.ClientConfig.CRYPTO_KEY_SIZE));
      this.updateProgressListeners(60);
      IOUtils.writeJSONToFile(CryptoUtils.keyPairToJSON(kp), BASE_PATH + id + ConfigFiles.ClientConfig.CRYPTO_KP_FILENAME);
      this.updateProgressListeners(80);
      IOUtils.writeJSONToFile(CryptoUtils.publicKeyToJSON(kp.getPublic()), BASE_PATH + id
          + ConfigFiles.ClientConfig.CRYPTO_PK_FILENAME);

      logger.info("Finished generating Crypto KeyPair");

      this.conf.setStringParameter(ConfigFiles.ClientConfig.ENCRYPTION_KEY_STORE, BASE_PATH + id
          + ConfigFiles.ClientConfig.CRYPTO_KP_FILENAME);
      this.updateProgressListeners(100);
    }
    catch (KeyGenerationException e) {
      logger.error("Exception during generation of Crypto KeyPair", e);
      throw new ClientException("Exception during generation of Crypto KeyPair", e);
    }
    catch (JSONIOException e) {
      logger.error("Exception during generation of Crypto KeyPair", e);
      throw new ClientException("Exception during generation of Crypto KeyPair", e);
    }
    catch (CryptoIOException e) {
      logger.error("Exception during generation of Crypto KeyPair", e);
      throw new ClientException("Exception during generation of Crypto KeyPair", e);
    }
    catch (JSONException e) {
      logger.error("Exception during generation of Crypto KeyPair", e);
      throw new ClientException("Exception during generation of Crypto KeyPair", e);
    }
  }

  /**
   * Generates a signing key using the parameters in the configuration file. The signing key will be stored as the id of this device
   * and the following "SigningKey.json". This will also generate a Certificate Signing Request that should be sent to the CA for
   * signing. The sending is not automatic.
   * 
   * @throws ClientException
   */
  public void generateSigningKey() throws ClientException {
    try {
      // Generate a KeyPair and save them
      logger.info("About to generate Signing Key");

      this.updateProgressListeners(0);
      this.updateProgressListeners(3);
      // Generate a signing key
      BLSKeyPair kp = BLSKeyPair.generateKeyPair();
      this.updateProgressListeners(10);
      // Generate SSL key
      KeyPair kpSSL = GenerateSignatureKeyAndCSR.generateKeyPair(SignatureType.RSA);

      // Create a new keystore
      TVSKeyStore ks = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      ks.initKeyStore();

      this.updateProgressListeners(20);

      String id = this.conf.getStringParameter(ConfigFiles.ClientConfig.ID);

      logger.info("Signing key generated, saving to {}", BASE_PATH + id + ConfigFiles.ClientConfig.SIGNING_KEY_FILENAME);
      ks.addBLSKeyPair(kp, ConfigFiles.ClientConfig.SIGNING_KEY_ALIAS);
      // IOUtils.writeJSONToFile(CryptoUtils.keyPairToJSON(kp), id + ConfigFiles.ClientConfig.SIGNING_KEY_FILENAME);

      this.updateProgressListeners(40);
      // Prepare the distinguished name for the certificate from any available parameters - excluding common name
      StringBuffer sb = new StringBuffer();
      if (this.conf.hasParam(ConfigFiles.ClientConfig.ORGANISATION)) {
        this.addToDistinguishedName(sb, ClientConstants.X509.ORGANISATION,
            this.conf.getStringParameter(ConfigFiles.ClientConfig.ORGANISATION));
      }
      if (this.conf.hasParam(ConfigFiles.ClientConfig.ORGANISATION_UNIT)) {
        this.addToDistinguishedName(sb, ClientConstants.X509.ORGANISATION_UNIT,
            this.conf.getStringParameter(ConfigFiles.ClientConfig.ORGANISATION_UNIT));
      }
      if (this.conf.hasParam(ConfigFiles.ClientConfig.COUNTRY)) {
        this.addToDistinguishedName(sb, ClientConstants.X509.COUNTRY,
            this.conf.getStringParameter(ConfigFiles.ClientConfig.COUNTRY));
      }
      if (this.conf.hasParam(ConfigFiles.ClientConfig.LOCATION)) {
        this.addToDistinguishedName(sb, ClientConstants.X509.LOCATION,
            this.conf.getStringParameter(ConfigFiles.ClientConfig.LOCATION));
      }
      if (this.conf.hasParam(ConfigFiles.ClientConfig.STATE)) {
        this.addToDistinguishedName(sb, ClientConstants.X509.STATE, this.conf.getStringParameter(ConfigFiles.ClientConfig.STATE));
      }

      // Prepare the final distinguished name values for the two keys by creating relevant Common Names for the two keys
      StringBuffer sbSSL = new StringBuffer();

      // Add the generic CN= to both
      sbSSL.append(ClientConstants.X509.COMMON_NAME);
      sbSSL.append("=");
      sbSSL.append(id + ConfigFiles.ClientConfig.SSL_SUFFIX);
      sbSSL.append(sb.toString());

      this.updateProgressListeners(60);

      // logger.info("Created Signing DN: {}", sbGen.toString());
      logger.info("Created SSL DN: {}", sbSSL.toString());

      // Create the CSR and save it to a file
      String csrSSL = GenerateSignatureKeyAndCSR.generateCertificateSigningRequest(sbSSL.toString(), kpSSL,
          this.conf.getIntParameter(ConfigFiles.ClientConfig.CERTIFICATE_LIFE), SignatureType.RSA, Client.CHALLENGE_PASSWORD);
      IOUtils.writeStringToFile(csrSSL, BASE_PATH + id + ConfigFiles.ClientConfig.SSL_CSR);
      logger.info("Generated SSL CSR and saved in {}", id + ConfigFiles.ClientConfig.SSL_CSR);
      this.updateProgressListeners(80);
      // Update the configuration file

      // The SSL key needs to be stored in a KeyStore, so we need to generate a self-signed certificate
      logger.info("Generating self-signed certificate as placeholder for KeyStore");
      ContentSigner sigGen = new JcaContentSignerBuilder(TVSSignature.getSignatureTypeString(SignatureType.RSA)).setProvider(
          CryptoUtils.BC_PROV).build(kpSSL.getPrivate());

      // This is just a place holder, hence it is fixed - we start the certificate 1 day before and finish 1 year later
      Date startDate = new Date(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1));
      Date endDate = new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(365));

      // Build the x.509 certificate for the SSL key
      X509v1CertificateBuilder v1CertGen = new JcaX509v1CertificateBuilder(new X500Principal(sbSSL.toString()), BigInteger.ONE,
          startDate, endDate, new X500Principal(sbSSL.toString()), kpSSL.getPublic());
      X509CertificateHolder certHolder = v1CertGen.build(sigGen);

      // Convert x.509 ceritificate into correct format for KeyStore
      JcaX509CertificateConverter conv = new JcaX509CertificateConverter().setProvider(CryptoUtils.BC_PROV);
      ks.getKeyStore().setKeyEntry(id + ConfigFiles.ClientConfig.SSL_SUFFIX, kpSSL.getPrivate(),
          Client.CHALLENGE_PASSWORD.toCharArray(), new Certificate[] { conv.getCertificate(certHolder) });

      logger.info("Finished generating self-signed certificate");
      // Set the values in the config file
      this.conf.setStringParameter(ConfigFiles.ClientConfig.SIGNING_KEY_STORE, BASE_PATH + id
          + ConfigFiles.ClientConfig.SIGNING_KEY_FILENAME);
      this.conf.setStringParameter(ConfigFiles.ClientConfig.SSL_KEYSTORE_LOCATION, BASE_PATH + id
          + ConfigFiles.ClientConfig.SSL_KEYSTORE_FILENAME);
      this.conf.setStringParameter(ConfigFiles.ClientConfig.SSL_CSR_LOCATION, BASE_PATH + id + ConfigFiles.ClientConfig.SSL_CSR);
      ks.store(BASE_PATH + id + ConfigFiles.ClientConfig.SIGNING_KEY_FILENAME, BASE_PATH + id
          + ConfigFiles.ClientConfig.SSL_KEYSTORE_FILENAME, Client.CHALLENGE_PASSWORD.toCharArray());
      this.updateProgressListeners(100);
    }
    catch (IOException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (JSONIOException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (JSONException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (CertificateRequestGenException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (KeyGenerationException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (KeyStoreException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (CertificateException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (OperatorCreationException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Exception whilst generating signing key and CSR", e);
      throw new ClientException("Exception during Signing Key and CSR generation", e);
    }
  }

  /**
   * Gets the folder where the certificates are stored, as specified in the config file. If it doesn't exist a new folder will be
   * created.
   * 
   * @return File that represents the folder for storing certificates
   * @throws IOException
   */
  public File getCertsLocation() throws IOException {
    File certFolder = new File(this.conf.getStringParameter(ClientConfig.CERTS_LOCATION));
    IOUtils.checkAndMakeDirs(certFolder);

    return certFolder;
  }

  /**
   * Gets the acceptable cipher suites
   * 
   * @return String array of acceptable cipher suites
   */
  public String[] getCipherSuite() {
    return this.cipherSuites;
  }

  public RaceDefs getRaceDefs() {
    return this.raceDefs;
  }

  /**
   * Gets the ConfigFile representing the client configuration
   * 
   * @return ConfigFile for the client configuration
   */
  public ConfigFile getClientConf() {
    return this.conf;
  }

  /**
   * Gets the CSR filename for this client, based on the constant CSR_FILENAME and the ID of this device as set in the config
   * 
   * @return String of the CSR Filename
   */
  public String getCSRFileName() {
    return this.conf.getStringParameter(ConfigFiles.ClientConfig.ID) + ConfigFiles.ClientConfig.CSR_FILENAME;
  }

  /**
   * Gets the BLSPublicKey as a TVSKeyStore JSONObject
   * 
   * @return String containing the JSON BLSKeyStore of the BLS Public Key
   * @throws ClientException
   */
  public String getJSONPublicKey() throws ClientException {
    try {
      String tvsKeyStoreFile = BASE_PATH + this.conf.getStringParameter(ConfigFiles.ClientConfig.ID)
          + ConfigFiles.ClientConfig.SIGNING_KEY_FILENAME;
      TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStore.load(tvsKeyStoreFile, Client.CHALLENGE_PASSWORD.toCharArray());

      TVSKeyStore publicKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      publicKeyStore.addBLSPublicKey(tvsKeyStore.getBLSPublicKey(ConfigFiles.ClientConfig.SIGNING_KEY_ALIAS),
          this.conf.getStringParameter(ConfigFiles.ClientConfig.ID));
      return publicKeyStore.getBLSKeyStore().toString();

    }
    catch (KeyStoreException e) {
      throw new ClientException("Exception whilst getting JSON Public Key", e);
    }

  }

  /**
   * Gets the next task or if it is ready to run, it returns Initialised.
   * 
   * @return String of the status or "Initialised" if completed
   */
  public String getNextTask() {
    this.checkStatus();

    if (!this.isInitialised()) {
      return this.statusParams[this.statusLevel];
    }
    else {
      return Client.INITIALISED_PARAM;
    }
  }

  /**
   * Gets the SSLSocketFactory for this Client
   * 
   * @return SSLSocketFactory to use for communicating with the MBB
   */
  public SSLSocketFactory getSocketFactory() {
    return this.factory;
  }

  /**
   * Gets the SSL CSR filename for this client, based on the constant CSR_FILENAME and the ID of this device as set in the config
   * 
   * @return String of the CSR filename
   */
  public String getSSLCSRFileName() {
    return this.conf.getStringParameter(ConfigFiles.ClientConfig.SSL_CSR_LOCATION);
  }

  /**
   * Checks what the status is and returns it.
   * 
   * @return the current status level - index of statusParams
   */
  public int getStatus() {
    this.checkStatus();

    return this.statusLevel;
  }

  /**
   * Gets the TimeoutManager so Servlets can add tasks into it
   * 
   * @return TimeoutManager for this client
   */
  public FileBackedTimeoutManager getTimeoutManager() {
    return this.timeoutManager;
  }

  public void addBallotTimeout(String ballotID) {
    this.getTimeoutManager().addDefaultTimeout(new DeleteBallotWorker(ballotID));
  }

  /**
   * Gets the ClientType of the current instance
   * 
   * @return ClientType of this client
   */
  public ClientType getType() {
    return this.type;
  }

  /**
   * Updates the certificate field in the config file.
   * 
   * Longer term we might need to update this to import it into the JKS.
   * 
   * @param certLocation
   *          the location of the certificate to import
   * @throws ClientException
   */
  public void importSSLCertificate(String certLocation) throws ClientException {
    try {
      this.conf.setStringParameter(ConfigFiles.ClientConfig.CERTIFICATE_LOCATION, certLocation);
      String caCert = this.conf.getStringParameter(ConfigFiles.ClientConfig.CA_CERT_LOCATION);

      String id = this.conf.getStringParameter(ConfigFiles.ClientConfig.ID);
      String keyStoreLocation = this.conf.getStringParameter(ConfigFiles.ClientConfig.SSL_KEYSTORE_LOCATION);
      KeyGeneration.importCertificates(certLocation, caCert, id + ClientConfig.SSL_SUFFIX, keyStoreLocation, "");
      logger.info("Importing SSL certificate {}", certLocation);
      KeyGeneration.importCertificateFiles(keyStoreLocation,
          new String[] { this.conf.getStringParameter(ConfigFiles.ClientConfig.PEER_CA_CERT_LOCATION) });
      logger.info("Importing Peer CA certificate {}", certLocation);
      this.initComms();
    }
    catch (CertificateStoringException e) {
      logger.error("Exception during import of SSL certificate", e);
      throw new ClientException("Exception during import of SSL certificate", e);
    }
    catch (PeerSSLInitException e) {
      logger.error("Exception during import of SSL certificate", e);
      throw new ClientException("Exception during import of SSL certificate", e);
    }
    catch (JSONIOException e) {
      logger.error("Exception during import of SSL certificate", e);
      throw new ClientException("Exception during import of SSL certificate", e);
    }
    catch (JSONException e) {
      logger.error("Exception during import of SSL certificate", e);
      throw new ClientException("Exception during import of SSL certificate", e);
    }
  }

  /**
   * Increments the progress with the float increment. This will only fire an updateProgressListeners call if the integer progress
   * will increase following the float increment.
   * 
   * @param increment
   *          float increment amount to add to the total
   */
  public void incrementProgress(float increment) {
    // Update progress total
    this.progress = this.progress + increment;

    // Round the value to an integer
    int tProgress = Math.round(this.progress);

    // Check if that results in a change to the integer progress, notify listeners if it does, if not do nothing
    if (tProgress != this.lastProgress) {
      this.lastProgress = tProgress;
      this.updateProgressListeners(this.lastProgress);
    }
  }

  /**
   * Initialises the communications components for this client. This is necessary because of the use of SSL, without SSL the
   * communication is simpler. This will only run if the client has the necessary keys in place - which when first run will not be
   * the case. After importing a new SSLCertificate this will also be called to re-initialise the communications with the newly
   * created certificates
   * 
   * @throws PeerSSLInitException
   */
  private void initComms() throws PeerSSLInitException {
    try {

      if (this.conf.hasParam(ClientConfig.SSL_KEYSTORE_LOCATION)) {
        logger.info("Initialising SSL Communications");
        SSLContext ctx = SSLContext.getInstance("TLS");
        KeyStore ks = CryptoUtils.loadKeyStore(this.conf.getStringParameter(ClientConfig.SSL_KEYSTORE_LOCATION));

        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(ks, "".toCharArray());
        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        this.factory = ctx.getSocketFactory();
        if (this.conf.hasParam(ClientConfig.CIPHER_SUITE)) {
          this.cipherSuites = new String[] { this.conf.getStringParameter(ClientConfig.CIPHER_SUITE) };
        }
      }
    }
    catch (NoSuchAlgorithmException e) {
      throw new PeerSSLInitException("Exception creating SSL comms factory", e);
    }
    catch (CryptoIOException e) {
      throw new PeerSSLInitException("Exception creating SSL comms factory", e);
    }
    catch (KeyStoreException e) {
      throw new PeerSSLInitException("Exception creating SSL comms factory", e);
    }
    catch (UnrecoverableKeyException e) {
      throw new PeerSSLInitException("Exception creating SSL comms factory", e);
    }
    catch (KeyManagementException e) {
      throw new PeerSSLInitException("Exception creating SSL comms factory", e);
    }
  }

  /**
   * Checks if the client is initialised.
   * 
   * @return true if the current is initialised (ballots generated), false if not
   */
  public boolean isInitialised() {
    this.checkStatus();

    return this.finishedInit;
  }

  /**
   * Removes a ProgressListener from this class
   * 
   * @param listener
   *          ProgressListener to remove
   */
  public void removeProgressListener(ProgressListener listener) {
    this.progressListeners.remove(listener);
  }

  /**
   * Called to update any ProgressListeners with a new value. The value they receive is the parameter
   * 
   * @param value
   *          to pass to the ProgressListeners
   */
  private void updateProgressListeners(int value) {
    for (ProgressListener pl : this.progressListeners) {
      pl.updateProgress(value);
    }
  }
}
