/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIERROR;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIMessage;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants.UIResponse;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * Servlet used to handle heartbeat messages.
 * 
 * @author Chris Culnane
 * 
 * @version $Revision: 1.0 $
 */
public class MBBHeartbeatProxyServlet implements NanoServlet {

  /**
   * Configuration file for the election information.
   */
  private ConfigFile              conf;

  /**
   * Logger
   */
  private static final Logger     logger              = LoggerFactory.getLogger(MBBHeartbeatProxyServlet.class);

  /**
   * Path to the schema file
   */
  private static final String     SCHEMA_LIST_PATH    = "/sdcard/vvoteclient/schemas/mbbproxyschemalist.json";

  /**
   * Constant string to refer to the general schema
   */
  private static final String     HEARTBEAT_SCHEMA_ID = "heartbeat";

  /**
   * HashMap that holsd a list of schemas indexed by type
   */
  private HashMap<String, String> schemas;

  /**
   * Client object that this servlet interacts with
   */
  private Client                  client;

  /**
   * TVSKeyStore that contains the certificates to verify the message from the MBB
   */
  private TVSKeyStore             clientKeyStore;

  /**
   * Constructor requiring the configuration file.
   * 
   * @param client
   *          Client that this servlet interacts with
   * 
   * @throws JSONIOException
   * @throws CryptoIOException
   */
  public MBBHeartbeatProxyServlet(Client client) throws JSONIOException, CryptoIOException {
    super();

    logger.info("Loading MBBHeartbeatProxyServlet");
    this.client = client;
    this.conf = this.client.getClientConf();
    this.schemas = ProxyServer.loadSchemas(MBBHeartbeatProxyServlet.SCHEMA_LIST_PATH);
    this.clientKeyStore = CryptoUtils.loadTVSKeyStore(this.client.getClientConf().getStringParameter(
        ClientConfig.SYSTEM_CERTIFICATE_STORE));
    logger.info("Finished loading MBBHeartbeatProxyServlet");
  }

  /**
   * Processes a Heartbeat message that is sent when checking connection to peers
   * 
   * @param message
   *          String representation of JSON message object
   * 
   * 
   * @return JSONObject with the WBB authorisation signatures
   * @throws JSONException
   * @throws IOException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws CertificateException
   * @throws UnrecoverableKeyException
   * @throws InvalidKeyException
   * @throws SignatureException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws MBBCommunicationException
   * @throws ProxyMessageProcessingException
   * @throws ConsensusException
   */
  private JSONObject processHeartbeatMessage(String message) throws JSONException, IOException, KeyStoreException,
      NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, InvalidKeyException, SignatureException,
      CryptoIOException, TVSSignatureException, JSONIOException, MBBCommunicationException, ProxyMessageProcessingException,
      ConsensusException {
    JSONObject messageSent = new JSONObject(message);

    TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    tvsKeyStore.load(this.conf.getStringParameter(ClientConfig.SIGNING_KEY_STORE), "".toCharArray());

    TVSSignature submitSig = new TVSSignature(SignatureType.BLS, tvsKeyStore.getBLSPrivateKey(ClientConfig.SIGNING_KEY_ALIAS));
    submitSig.update(messageSent.getString(UIMessage.TYPE));

    messageSent.put(JSONWBBMessage.SENDER_ID, this.conf.getStringParameter(ClientConfig.ID));
    messageSent.put(JSONWBBMessage.SENDER_SIG, submitSig.signAndEncode(EncodingType.BASE64));
    MBBConfig mbbConf = new MBBConfig(this.conf.getStringParameter(ClientConfig.MBB_CONF_FILE));

    StringBuffer dataToCheck = new StringBuffer();
    dataToCheck.append(messageSent.getString(UIMessage.TYPE));

    JSONArray mbbResponse = MBB.sendMessageWaitForSigs(mbbConf, messageSent.toString(), this.client.getSocketFactory(),
        this.client.getCipherSuite());

    TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, this.clientKeyStore);
    tvsSig.update(dataToCheck.toString());

    JSONObject response = tvsSig.verifyAndCombine(mbbResponse, SystemConstants.SIGNING_KEY_SK2_SUFFIX, this.client.getClientConf()
        .getIntParameter(ClientConfig.THRESHOLD), mbbConf.getPeers().size(), true, true);
    messageSent.put("peers", mbbConf.getPeers());
    logger.info("Received response, will check signature");
    logger.info("Signature on response verified");
    if (response.has(TVSSignature.COMBINED)) {
      messageSent.remove(JSONWBBMessage.SENDER_ID);
      messageSent.remove(JSONWBBMessage.SENDER_SIG);
      messageSent.put(UIResponse.WBB_SIGNATURES, response.getString(TVSSignature.COMBINED));
      messageSent.put(UIERROR.WBB_RESPONSES, mbbResponse);
    }
    else {
      messageSent = ProxyServer.constructErrorMessage(mbbResponse, "Exception when sending message to MBB");
      logger.error("Exception in sending message");
    }

    return messageSent;
  }

  /**
   * Runs the servlet.
   * 
   * 
   * @param uri
   *          String
   * @param method
   *          Method
   * @param header
   *          Map<String,String>
   * @param params
   *          Map<String,String>
   * @param files
   *          Map<String,String>
   * @param homeDir
   *          File
   * @return Response
   * 
   * @see uk.ac.surrey.cs.tvs.comms.http.NanoServlet#runServlet(java.lang.String, fi.iki.elonen.NanoHTTPD.Method, java.util.Map,
   *      java.util.Map, java.util.Map, java.io.File)
   */
  @Override
  public Response runServlet(String uri, Method method, Map<String, String> header, Map<String, String> params,
      Map<String, String> files, File homeDir) {
    if (params.containsKey(ClientConstants.REQUEST_MESSAGE_PARAM)) {
      try {
        if (!JSONUtils.validateSchema(this.schemas.get(HEARTBEAT_SCHEMA_ID), params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
        JSONObject msg = new JSONObject(params.get(ClientConstants.REQUEST_MESSAGE_PARAM));
        if (JSONUtils.validateSchema(this.schemas.get(msg.get(ClientConstants.UIMessage.TYPE)),
            params.get(ClientConstants.REQUEST_MESSAGE_PARAM))) {
          if (msg.get(ClientConstants.UIMessage.TYPE).equals("heartbeat")) {
            return new Response(Response.Status.OK, VVoteNanoHTTPD.MIME_JSON, this.processHeartbeatMessage(
                params.get(ClientConstants.REQUEST_MESSAGE_PARAM)).toString());
          }
          else {
            return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Unknown message type");
          }
        }
        else {
          return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT, "Message validation failed");
        }
      }
      catch (UnrecoverableKeyException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (InvalidKeyException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (KeyStoreException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (NoSuchAlgorithmException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (CertificateException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (SignatureException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (JSONException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (IOException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (CryptoIOException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (TVSSignatureException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (JSONIOException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (MBBCommunicationException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (ProxyMessageProcessingException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
      catch (ConsensusException e) {
        return new Response(Response.Status.INTERNAL_ERROR, VVoteNanoHTTPD.MIME_PLAINTEXT, "Exception whilst processing message:"
            + e.getMessage());
      }
    }
    else {
      return new Response(Response.Status.BAD_REQUEST, VVoteNanoHTTPD.MIME_PLAINTEXT,
          "Missing msg parameter - no message to process");
    }
  }
}
