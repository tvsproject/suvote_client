/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.prng.FixedSecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandom;
import org.bouncycastle.crypto.prng.SP800SecureRandomBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.AuditFileCreationException;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.utils.comparators.SerialNumberComparator;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Utility class for generating initial inputs for a mix and creating an audit file.
 * 
 * We would expect the candidate IDs to be agreed and published to the WBB in advance of the election.
 * 
 * @author Chris Culnane
 * 
 */
public class BallotGenUtils {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(BallotGenUtils.class);

  /**
   * Creates an audit file, which contains a list of serial number to audit, ready for performing the audit. We keep this as a
   * separate method to the actual audit to allow the audit to be run using an externally generated list.
   * 
   * The serialNoFile can be any file containing one JSONObject per line, within which a serialNo field is specified. As such, the
   * file of submitted ciphers can be utilised for this purpose.
   * 
   * @param seed
   *          byte array of random bytes - typically the output from a FiatShamir hash
   * @param serialNoFile
   *          String path to file containing JSONObject that contain the serial number
   * @param ballotsToAudit
   *          the number of ballots to audit
   * @param outputFile
   *          String path to the output location to store the list of serial numbers that need auditing
   * @param personalizationString
   *          Personalization string to use during DRBG construction - should be committed value - for example, device ID
   * @throws IOException
   * @throws JSONException
   * @throws JSONIOException
   * @throws AuditFileCreationException
   */
  public static void createAuditFile(byte[] seed, String serialNoFile, int ballotsToAudit, String outputFile,
      String personalizationString) throws AuditFileCreationException {

    logger.info("Reading serialNo's from {}", serialNoFile);
    try {
      ArrayList<String> serialNumbers = new ArrayList<String>();

      BufferedReader br = null;
      try {
        br = new BufferedReader(new FileReader(serialNoFile));
        String line;
        while ((line = br.readLine()) != null) {
          JSONObject obj = new JSONObject(line);
          serialNumbers.add(obj.getString(MessageFields.Ballot.SERIAL_NO));
        }
      }
      finally {
        if (br != null) {
          br.close();
        }
      }
      // Check we aren't trying to audit more ballots than exist
      if (ballotsToAudit > serialNumbers.size()) {
        throw new AuditFileCreationException("Trying to audit more ballots than exist" + ballotsToAudit + ","
            + serialNumbers.size());
      }

      // Use DRBG from BouncyCastle to prepare source of randomness from hash output
      FixedSecureRandom fsr = new FixedSecureRandom(seed);

      // We disable predictionResistant since this would require re-seeding and we only have a finite amount of seed randomness
      SP800SecureRandomBuilder rBuild = new SP800SecureRandomBuilder(fsr, false);
      rBuild.setPersonalizationString(personalizationString.getBytes());
      SP800SecureRandom drbg = rBuild.buildHash(new SHA256Digest(), null, false);

      // We shuffle and then take the first n values as those to be audited
      Collections.shuffle(serialNumbers, drbg);
      ArrayList<String> finalSerials = new ArrayList<String>();

      for (int i = 0; i < ballotsToAudit; i++) {
        finalSerials.add(serialNumbers.get(i));
      }

      // Create a JSON file to store the values in
      JSONObject auditList = new JSONObject();
      JSONArray serials = new JSONArray();

      // Resort the list so it is in order - this is more efficient when conducting the actual audit
      Collections.sort(finalSerials, new SerialNumberComparator());
      for (String serial : finalSerials) {
        serials.put(serial);
      }
      auditList.put(ClientConstants.AuditFile.AUDIT_SERIALS, serials);

      logger.info("Saving audit file to: {}", outputFile);
      IOUtils.writeJSONToFile(auditList, outputFile);
    }
    catch (IOException e) {
      throw new AuditFileCreationException("An exception occured whilst creating the audit file", e);
    }
    catch (JSONException e) {
      throw new AuditFileCreationException("An exception occured whilst creating the audit file", e);
    }
    catch (JSONIOException e) {
      throw new AuditFileCreationException("An exception occured whilst creating the audit file", e);
    }
  }

  /**
   * Takes the JSONArray of encrypted Candidate IDs and creates a line for each ballot to be generated. This is equivalent to
   * creating a row per ballot, with the columns represented by the JSONArray. We pre-compute this to make the mix code generic, so
   * it could be performing the mix after a previous mix, as opposed to a single mix.
   * 
   * @param numberOfBallots
   *          the number of rows (ballots) to create in the output
   * @param candIDsFile
   *          JSONArray in a file, containing encrypted Candidate IDs
   * @param outputFile
   *          location to save output to
   * @throws JSONIOException
   * @throws FileNotFoundException
   */
  public static void createInputCiphers(int numberOfBallots, String candIDsFile, String outputFile) throws JSONIOException,
      FileNotFoundException {

    logger.info("Loading Encrypted Candidate IDs from {}", candIDsFile);
    JSONArray candIDs = IOUtils.readJSONArrayFromFile(candIDsFile);

    logger.info("Outputting file of initial inputs contain {} rows of Encrypted Candidate IDs to {}", numberOfBallots, outputFile);
    PrintWriter pw = new PrintWriter(outputFile);
    try {
      String line = candIDs.toString();
      for (int i = 0; i < numberOfBallots; i++) {
        pw.println(line);
      }
    }
    finally {
      pw.close();
    }
  }

  /**
   * Creates message and the required signature for sending the Ciphers to the MBB.
   * 
   * @param conf
   *          ConfigFile to use for creating and sending the message
   * @param type
   *          String of the type of message - set a type in JSON message
   * @param fileDigest
   *          MessageDigest containing digest of the file we are about to send
   * @param fileSize
   *          the size of the file
   * @return JSONObject containing the constructed message
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONException
   * @throws JSONIOException
   * @throws KeyStoreException 
   */
  public static JSONObject createMBBMessage(ConfigFile conf, String type, MessageDigest fileDigest, long fileSize)
      throws CryptoIOException, TVSSignatureException, JSONException, JSONIOException, KeyStoreException {
    // Get a signature object
    TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    tvsKeyStore.load(conf.getStringParameter(ClientConfig.SIGNING_KEY_STORE), "".toCharArray());

    // PrivateKey sigKey = CryptoUtils.jsonToSigningPrivateKey(
    // IOUtils.readJSONObjectFromFile(conf.getStringParameter(ClientConfig.SIGNING_KEY_STORE)), SignatureType.DEFAULT);

    TVSSignature submitSig = new TVSSignature(SignatureType.BLS, tvsKeyStore.getBLSPrivateKey(ClientConfig.SIGNING_KEY_ALIAS));

    // Create submissionid = serialno
    String subID = UUID.randomUUID().toString();
    submitSig.update(subID);
    byte[] digest = fileDigest.digest();
    String digestString = IOUtils.encodeData(EncodingType.BASE64, digest);
    submitSig.update(digestString);

    // Create JSON message
    JSONObject message = new JSONObject();
    message.put(JSONWBBMessage.TYPE, type);
    message.put(FileMessage.SENDER_ID, conf.getStringParameter(ClientConfig.ID));
    message.put(FileMessage.ID, subID);
    message.put(FileMessage.SENDER_SIG, submitSig.signAndEncode(EncodingType.BASE64));
    message.put(FileMessage.DIGEST, digestString);
    message.put(FileMessage.FILESIZE, fileSize);

    return message;
  }
}
