/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipOutputStream;

import javax.net.ssl.SSLSocketFactory;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.AuditFileCreationException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenAuditException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenerationException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.EndOfFileException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberException;
import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.BallotGenConfig;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CommitException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Performs the actual mixing of the ballot ciphers.
 * 
 * @author Chris Culnane
 * 
 */
public class BallotGenMix {

  /**
   * Creates new default ECGroup to represent underlying group
   */
  private ECUtils                group             = new ECUtils();

  /**
   * ECPoint that represents the public key
   */
  private ECPoint                pk;

  /**
   * BallotGen Configuration
   */
  private BallotGenConf          conf;

  /**
   * List of base candidate IDs, these are encrypted with a fixed randomness
   */
  private List<CandidateId>      baseCandidates;

  /**
   * Logger
   */
  private static final Logger    logger            = LoggerFactory.getLogger(BallotGenMix.class);

  /**
   * RandomnessFiles class to find and keep reference to all the randomness files
   */
  private RandomnessFiles        randomnessFiles;

  /**
   * JSONObject containing ElGamal key pair
   */
  private JSONObject             myKeyPair;

  /**
   * SSLSocketFactory to use for MBB Communication
   */
  private SSLSocketFactory       factory           = null;

  /**
   * String array of acceptable cipher suites
   */
  private String[]               cipherSuites      = null;

  /**
   * ConfigFile containing client configuration
   */
  private ConfigFile             clientConfig;

  /**
   * Current progress
   */
  private float                  progress          = 0f;

  /**
   * Last integer progress amount, used to increment an integer progress listener with a float based progress value
   */
  private int                    lastProgress      = 0;

  /**
   * List of ProgressListeners
   */
  private List<ProgressListener> progressListeners = new ArrayList<ProgressListener>();

  /**
   * Construct a new BallotGenMix that will be used to generate ballots.
   * 
   * @param clientConf
   *          Client config file, used to configure BallotGenMix and access keys
   * @throws JSONException
   * @throws IOException
   * @throws JSONIOException
   * @throws ClientException
   * @throws PeerSSLInitException
   */
  public BallotGenMix(ConfigFile clientConf) throws JSONIOException, JSONException, IOException, ClientException {
    super();

    logger.info("Created BallotGenMix object");
    this.clientConfig = clientConf;
    this.conf = new BallotGenConf(this.clientConfig.getStringParameter(ConfigFiles.ClientConfig.BALLOT_GEN_CONF));
    this.randomnessFiles = new RandomnessFiles(this.clientConfig.getStringParameter(ConfigFiles.ClientConfig.RANDOMNESS_FOLDER));
    this.randomnessFiles.findRandomnessFiles();
    if (!this.randomnessFiles.verifyAllFilesAndSignatures(this.clientConfig)) {
      logger.warn("Some files were invalid, but sufficient valid files were received. Will continue with only valid files");
    }

    this.pk = this.group.pointFromJSON(IOUtils.readJSONObjectFromFile(this.clientConfig
        .getStringParameter(ConfigFiles.ClientConfig.ELECTION_PK)));
    this.baseCandidates = this.loadEncryptedCandIds(this.clientConfig
        .getStringParameter(ConfigFiles.ClientConfig.BASE_ENCRYPTED_IDS));
    this.myKeyPair = IOUtils.readJSONObjectFromFile(this.clientConfig
        .getStringParameter(ConfigFiles.ClientConfig.ENCRYPTION_KEY_STORE));
    logger.info("Finished creating BallotGenMix object");
  }

  /**
   * Adds a ProgressListener to this class
   * 
   * @param listener
   *          ProgressListener to add
   */
  public void addProgressListener(ProgressListener listener) {
    this.progressListeners.add(listener);
  }

  /**
   * Generates an individual ballot using the various CombinedRandomness object and values. It returns a JSONArray with the
   * candidate permutations for the races.
   * 
   * @param combinedRand
   *          CombinedRandomness object to get the encryption randomness from
   * @param availProcs
   *          int of the number of processor cores to use
   * @param outputCiphers
   *          JSONArray to store the output ciphers into
   * @param combinedRandomness
   *          JSONArray of the combinedRandomness to use
   * 
   * @return JSONArray of ballot permutation
   * @throws JSONException
   * @throws RandomnessCommitmentException
   * @throws CombinedRandomnessException
   * @throws BallotGenerationException
   */
  private JSONArray generateBallot(CombinedRandomness combinedRand, int availProcs, JSONArray outputCiphers,
      JSONArray combinedRandomness) throws JSONException, RandomnessCommitmentException, CombinedRandomnessException,
      BallotGenerationException {
    JSONArray racePermutations = new JSONArray();

    int candidateCount = 0;
    // Step through each race specified in the configuration
    for (int candidatesInRace : this.conf.getRaceCounts()) {
      // Prepare a list for candidate ciphers
      ArrayList<CandidateCipherText> candidateCiphers = new ArrayList<CandidateCipherText>();

      // Prepare a thread pool with availProcs + 1
      ExecutorService pool = Executors.newFixedThreadPool(availProcs + 1);
      JSONArray raceRandomness = new JSONArray();

      // Step through each candidate
      for (int cipherCount = 0; cipherCount < candidatesInRace; cipherCount++, candidateCount++) {
        CandidateCipherText candCipher = this.baseCandidates.get(candidateCount).getNewCipherText();
        byte[] nextCombinedRandomValue = combinedRand.getNextCombinedValue();
        raceRandomness.put(IOUtils.encodeData(EncodingType.BASE64, nextCombinedRandomValue));

        // Defer the re-encryption till later
        candCipher.setReencryptData(this.pk, new BigInteger(1, nextCombinedRandomValue));
        candCipher.setPermutationIndex(cipherCount);

        // Execute the re-encryption - could be now or later
        pool.execute(candCipher);

        // Add the candidate cipher to the list
        candidateCiphers.add(candCipher);
      }

      combinedRandomness.put(raceRandomness);

      // Shutdown the thread pool - this initiates an orderly shutdown
      pool.shutdown();

      // We need to wait for the threads to finish - there isn't really a limit on this, it takes as long as it takes
      try {
        if (!pool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS)) {
          logger.error("thread pool timed out after {} seconds", Long.MAX_VALUE);
          throw new BallotGenerationException("The thread performing re-encryption timed out");
        }
      }
      catch (InterruptedException e) {
        logger.error("The thread performing re-encryption was interrupted", e);
        throw new BallotGenerationException("The thread performing re-encryption was interrupted", e);
      }

      // Perform the sort on the re-encrypted ciphers - this performs the actual permuting
      Collections.sort(candidateCiphers);

      // Prepare an output JSONArray for the permutation
      JSONArray outputPermutation = new JSONArray();

      for (CandidateCipherText candCipher : candidateCiphers) {
        outputCiphers.put(candCipher.getAsJSON());
        outputPermutation.put(candCipher.getPermutationIndex());
      }

      // Add this to the race array
      racePermutations.put(outputPermutation);
    }

    return racePermutations;
  }

  /**
   * Generates the ballots using the pre-computed randomness. It performs the re-encryption concurrently to reduce overall ballot
   * generation time. However, the rest of the processing happens on a single file.
   * 
   * @param outputDir
   *          String path to place the output files into (will be created if it doesn't exist)
   * @param cipherOutput
   *          String path to file where the ciphers should be saved to
   * @throws BallotGenerationException
   * @throws CombinedRandomnessException
   * @throws RandomnessCommitmentException
   */
  public void generateBallots(String outputDir, String cipherOutput) throws BallotGenerationException,
      RandomnessCommitmentException, CombinedRandomnessException {
    int ballotsToGenerate = this.conf.getIntParameter(ConfigFiles.BallotGenConfig.NUMBER_BALLOTS_TO_GENERATE);
    logger.info("Generating ballots, output directory is {}, output ciphers to {}", outputDir, cipherOutput);

    CombinedRandomness combinedRand = null;
    BufferedWriter ballotCiphers = null;

    JSONArray ballots = new JSONArray();
    try {
      // Create the output directory
      File outDir = new File(outputDir);
      IOUtils.checkAndMakeDirs(outDir);
      

      // Create the combined randomness object and move to the first row
      combinedRand = new CombinedRandomness(this.myKeyPair, this.randomnessFiles,
          this.clientConfig.getStringParameter(ClientConfig.AES_ALGORITHM));
      

      // Prepare an output file
      ballotCiphers = new BufferedWriter(new FileWriter(cipherOutput));

      // Get the number of processors
      int availProcs = Runtime.getRuntime().availableProcessors();

      // Calculate some progress values
      float increment = 80f / ballotsToGenerate;

      // Step through each ballot we need to create
      for (int ballotCounter = 0; ballotCounter < ballotsToGenerate; ballotCounter++) {
        combinedRand.nextRow();
        // Prepare values for ballot output
        JSONArray outputCiphers = new JSONArray();
        JSONArray combinedRandomness = new JSONArray();
        JSONArray racePermutations = this.generateBallot(combinedRand, availProcs, outputCiphers, combinedRandomness);

        // Construct the final ballot combining the different races
        JSONObject outputBallot = new JSONObject();
        outputBallot.put(MessageFields.Ballot.SERIAL_NO, combinedRand.getCurrentSerialNo());
        outputBallot.put(MessageFields.Ballot.PERMUTATIONS, racePermutations);
        outputBallot.put(MessageFields.Ballot.COMBINED_RANDOMNESS, combinedRandomness);
        StringBuffer commitPermutation = new StringBuffer();

        for (int i = 0; i < racePermutations.length(); i++) {
          commitPermutation.append(racePermutations.getJSONArray(i).join(MessageFields.PREFERENCE_SEPARATOR));
          commitPermutation.append(MessageFields.RACE_SEPARATOR);
        }

        byte[] witness = combinedRand.getNextCombinedValue();
        byte[] permutationCommit = CryptoUtils.generateCommitment(commitPermutation.toString().getBytes(), witness);

        String permutationCommitString = IOUtils.encodeData(EncodingType.BASE64, permutationCommit);
        outputBallot.put(MessageFields.Ballot.COMMIT_WITNESS, IOUtils.encodeData(EncodingType.BASE64, witness));

        // Write the ballot data to a new file in the output directory with the serial number as the file name
        String ballotFile = outputDir + File.separator + IOUtils.getFileNameFromSerialNo(combinedRand.getCurrentSerialNo())
            + MessageFields.Ballot.BALLOT_FILE_EXTENSION;
        ballots.put(ballotFile);
        IOUtils.writeJSONToFile(outputBallot, ballotFile);

        // Create a new cipher object for committing to the WBB
        JSONObject wbbCiphers = new JSONObject();
        wbbCiphers.put(MessageFields.WBBCipher.SERIAL_NO, combinedRand.getCurrentSerialNo());
        wbbCiphers.put(MessageFields.WBBCipher.CIPHERS, outputCiphers);
        wbbCiphers.put(MessageFields.WBBCipher.PERMUTATION, permutationCommitString);

        // Add to output file
        ballotCiphers.write(wbbCiphers.toString());
        ballotCiphers.newLine();

        // Update progress
        this.incrementProgress(increment);



      }

      IOUtils.writeJSONToFile(ballots, this.conf.getStringParameter(BallotGenConfig.BALLOT_LIST));
      JSONObject ballotDB = new JSONObject();
      ballotDB.put(ClientConstants.BallotDB.NEXT_BALLOT, 0);

      IOUtils.writeJSONToFile(ballotDB, this.conf.getStringParameter(BallotGenConfig.BALLOT_DB));
    }
    catch (IOException e) {
      throw new BallotGenerationException("Exception whilst running ballot generation: IOException", e);
    }
    catch (SerialNumberException e) {
      throw new BallotGenerationException("Exception whilst running ballot generation: SerialNumber Exception", e);
    }
    catch (JSONException e) {
      throw new BallotGenerationException("Exception whilst running ballot generation: IOException", e);
    }
    catch (JSONIOException e) {
      throw new BallotGenerationException("Exception whilst running ballot generation: IOException", e);
    }
    catch (CommitException e) {
      throw new BallotGenerationException("Exception whilst running ballot generation: Commit Exception", e);
    }
    catch (EndOfFileException e) {
      throw new BallotGenerationException("Exception whilst running ballot generation: End of Randomness File", e);
    }
    finally {
      if (ballotCiphers != null) {
        try {
          ballotCiphers.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing ballotCiphers", e);
        }
      }

      if (combinedRand != null) {
        combinedRand.closeAllFiles();
      }
    }
  }

  /**
   * Increments the progress with the float increment. This will only fire an updateProgressListeners call if the integer progress
   * will increase following the float increment.
   * 
   * @param increment
   *          float increment amount to add to the total
   */
  public void incrementProgress(float increment) {
    // Update progress total
    this.progress = this.progress + increment;

    // Round the value to an integer
    int tProgress = Math.round(this.progress);

    // Check if that results in a change to the integer progress, notify listeners if it does, if not do nothing
    if (tProgress != this.lastProgress) {
      this.lastProgress = tProgress;
      this.updateProgressListeners(this.lastProgress);
    }
  }

  /**
   * Loads the encrypted candidate IDs from the file.
   * 
   * @param encryptedCandIdFile
   *          String path to file containing JSONArray of candidate ids
   * @return List of CandidateId objects, one for each candidate ID
   * @throws JSONIOException
   * @throws JSONException
   */
  private List<CandidateId> loadEncryptedCandIds(String encryptedCandIdFile) throws JSONIOException, JSONException {
    logger.info("Loading encrypted candidate ids from {}", encryptedCandIdFile);

    JSONArray encryptedCandIds = IOUtils.readJSONArrayFromFile(encryptedCandIdFile);
    List<CandidateId> baseCandidates = new ArrayList<CandidateId>();

    for (int i = 0; i < encryptedCandIds.length(); i++) {
      baseCandidates.add(new CandidateId(encryptedCandIds.getJSONObject(i)));
    }

    return baseCandidates;
  }

  /**
   * Extracts the required information for the audit and submits it to the MBB. Note, this does not perform a crypto check on the
   * actual operations, that should be performed independently.
   * 
   * @param ballotGenOutput
   *          the JSONObject containing the summary of a ballot generation
   * @param wbbCiphers
   *          String path to ciphers file
   * @param auditIndexFile
   *          String path to save the file specifying the indexes to audit
   * @param auditOutput
   *          String path to output folder to store audit files into
   * @param keyPair
   *          String path to the Key for this device
   * @param randomnessDir
   *          String path to the top-level randomness directory
   * @param auditResponse
   *          String path to file to save response in
   * @param auditSubmissionFile
   *          String path to save audit submission in
   * @throws JSONException
   * @throws AuditFileCreationException
   * @throws BallotGenAuditException
   * @throws JSONIOException
   */
  public void performAudit(JSONObject ballotGenOutput, String wbbCiphers, String auditIndexFile, String auditOutput,
      String keyPair, String randomnessDir, String auditResponse, String auditSubmissionFile) throws JSONException,
      AuditFileCreationException, BallotGenAuditException, JSONIOException {
    // Create the seed from the output fiatShamir from the ballotGeneration
    byte[] seed = IOUtils.decodeData(EncodingType.BASE64,
        ballotGenOutput.getString(ClientConstants.BallotGenCommitResponse.FIAT_SHAMIR));

    // Create an audit file that contains the list of serial number to audit
    BallotGenUtils.createAuditFile(seed, wbbCiphers, this.conf.getIntParameter(BallotGenConfig.NUMBER_BALLOTS_TO_AUDIT),
        auditIndexFile, this.clientConfig.getStringParameter(ClientConfig.ID));
    this.incrementProgress(5);
    // Create the BallotGenAudit object, this will be the object that actually performs the audit
    BallotGenAudit amn = new BallotGenAudit(this.conf, keyPair, randomnessDir, this.clientConfig);
    amn.setSSLFactoryAndCipherSuite(this.factory, this.cipherSuites);
    for (ProgressListener pl : this.progressListeners) {
      amn.addProgressListener(pl);
    }
    amn.doAudit(auditIndexFile, auditOutput);

    // Submit audit to MBB
    JSONObject messageAndSigs = amn.submitAuditToWBB(auditOutput, auditSubmissionFile);
    this.incrementProgress(10);
    IOUtils.writeJSONToFile(messageAndSigs, auditResponse);
    this.incrementProgress(5);
  }

  /**
   * Removes a ProgressListener from this class
   * 
   * @param listener
   *          ProgressListener to remove
   */
  public void removeProgressListener(ProgressListener listener) {
    this.progressListeners.remove(listener);
  }

  /**
   * Sets the SSLSocketFactory and CipherSuites to use for MBB communication. If not set will revert to standard sockets, although
   * MBB will probably only accept SSL connections.
   * 
   * @param factory
   *          SSLSocketFactory to use for creating sockets
   * @param cipherSuite
   *          String array of acceptable cipher suites
   */
  public void setSSLFactoryAndCipherSuite(SSLSocketFactory factory, String[] cipherSuite) {
    this.factory = factory;
    this.cipherSuites = cipherSuite;
  }

  /**
   * Submits the file containing the ballot ciphers to the MBB and returns the response.
   * 
   * @param filename
   *          String path to file containing ballot ciphers
   * @param outputFile
   *          String path to zip file that should be created for submission
   * @return JSONObject containing the MBB responses
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONIOException
   * @throws MBBCommunicationException
   */
  public JSONObject submitBallotsToWBB(String filename, String outputFile) throws CryptoIOException, TVSSignatureException,
      JSONIOException, MBBCommunicationException {
    try {
      logger.info("About to submit ballot to MBB");
      // Get a reference to the cipher file
      File wbbSubFile = new File(outputFile);

      // Prepare a zip file for sending the data to the MBB
      ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(wbbSubFile));

      MessageDigest md = MessageDigest.getInstance(ClientConstants.DEFAULT_MESSAGE_DIGEST);
      logger.info("Creating zip file");
      try {
        // Add the ciphers file to the zip and close it
        ZipUtil.addFileToZip(new File(filename), md, zos);
        logger.info("Added {} to zip", filename);
      }
      finally {
        zos.close();
      }

      // Create the message
      logger.info("Creating MBB Submission Message");
      JSONObject message = BallotGenUtils.createMBBMessage(this.clientConfig, MessageTypes.BALLOT_GEN_COMMIT, md,
          wbbSubFile.length());

      // Get the config file to get a list of peers to send it to
      MBBConfig mbbConf = new MBBConfig(this.clientConfig.getStringParameter(ClientConfig.MBB_CONF_FILE));
      logger.info("Sending message");
      JSONArray responseArray = MBB.sendMessage(mbbConf,
          message.toString() + MBB.FILE_SUB_SEPARATOR + wbbSubFile.getAbsolutePath(), this.factory, this.cipherSuites);

      // Will wait for response and then check the responses
      if (!MBB.checkThresholdOfResponsesReceived(responseArray, this.clientConfig.getIntParameter(ClientConfig.THRESHOLD))) {
        logger
            .error("Either an error has occured or insufficient valid responses have been received: {}", responseArray.toString());
        throw new MBBCommunicationException("Exception whilst submitted to MBB, insufficient valid responses");
      }

      // Get the consensus commitTime - we need this for checking the signature
      String commitTime = MBB.getThresholdFieldValue(responseArray, MessageFields.PeerResponse.COMMIT_TIME,
          this.clientConfig.getIntParameter(ClientConfig.THRESHOLD));

      logger.info("Received response, will check signature");

      // Construct a signature for checking
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, CryptoUtils.loadTVSKeyStore(this.clientConfig
          .getStringParameter(ClientConfig.SYSTEM_CERTIFICATE_STORE)));
      tvsSig.update(message.getString(FileMessage.ID));
      tvsSig.update(message.getString(FileMessage.DIGEST));
      tvsSig.update(message.getString(FileMessage.SENDER_ID));
      tvsSig.update(commitTime);

      // Check the signature
      JSONObject response = tvsSig.verifyAndCombine(responseArray, SystemConstants.SIGNING_KEY_SK2_SUFFIX,
          this.clientConfig.getIntParameter(ClientConfig.THRESHOLD), mbbConf.getPeers().size(), true, true);

      // Check whether we received valid threshold of responses
      if (response.has(TVSSignature.COMBINED)) {
        // Prepare a fiatShamir value

        logger.info("Creating fiatShamir to conduct audit with");
        MessageDigest fiatShamir = MessageDigest.getInstance("SHA256", "BC");
        fiatShamir.update(this.clientConfig.getStringParameter(ClientConfig.ID).getBytes());
        fiatShamir.update(message.getString(FileMessage.ID).getBytes());
        fiatShamir.update(commitTime.getBytes());
        IOUtils.addFileIntoDigest(new File(filename), fiatShamir);

        String sigData = response.getString(TVSSignature.COMBINED);
        fiatShamir.update(IOUtils.decodeData(EncodingType.BASE64, sigData));

        // Get the bytes from the digest
        byte[] fsByte = fiatShamir.digest();

        // Create output object containing all the data needed to perform an audit
        JSONObject outData = new JSONObject();
        outData.put(ClientConstants.BallotGenCommitResponse.PEER_ID, this.clientConfig.getStringParameter(ClientConfig.ID));
        outData.put(ClientConstants.BallotGenCommitResponse.SUBMISSION_ID, message.getString(FileMessage.ID));
        outData.put(ClientConstants.BallotGenCommitResponse.BALLOT_FILE, filename);
        outData.put(ClientConstants.BallotGenCommitResponse.WBB_SIG, responseArray);
        outData.put(ClientConstants.BallotGenCommitResponse.FIAT_SHAMIR, IOUtils.encodeData(EncodingType.BASE64, fsByte));
        logger.info("Created output JSONObject for audit: {}", outData.toString());
        this.incrementProgress(20);
        return outData;
      }
      else {
        logger.error("Failed to submit to MBB {}", response);
        throw new MBBCommunicationException("Failed to submit to MBB, see log for further details");
      }
    }
    catch (IOException e) {
      throw new MBBCommunicationException("Exception when trying to upload ciphers to MBB", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new MBBCommunicationException("Exception when trying to upload ciphers to MBB", e);
    }
    catch (JSONException e) {
      throw new MBBCommunicationException("Exception when trying to upload ciphers to MBB", e);
    }
    catch (NoSuchProviderException e) {
      throw new MBBCommunicationException("Exception when trying to upload ciphers to MBB", e);
    }
    catch (ConsensusException e) {
      throw new MBBCommunicationException("Inconsistencies in response meant no threshold response could be found", e);
    }
    catch (KeyStoreException e) {
      throw new MBBCommunicationException("Exception when trying to upload ciphers to MBB", e);
    }
  }

  /**
   * Called to update any ProgressListeners with a new value. The value they receive is the parameter
   * 
   * @param value
   *          to pass to the ProgressListeners
   */
  private void updateProgressListeners(int value) {
    for (ProgressListener pl : this.progressListeners) {
      pl.updateProgress(value);
    }
  }
}
