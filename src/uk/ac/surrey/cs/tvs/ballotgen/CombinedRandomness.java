/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.EndOfFileException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.FindSerialNoException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.RandomnessCommitmentException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Combines the randomness from several different sources into a single randomness values to be used for re-encryption.
 * 
 * @author Chris Culnane
 * 
 */
public class CombinedRandomness {

  /**
   * Logger
   */
  private static final Logger logger          = LoggerFactory.getLogger(CombinedRandomness.class);

  /**
   * List of randomness rows, each representing the randomness row from a different source
   */
  private List<RandomnessRow> randomness      = new ArrayList<RandomnessRow>();

  /**
   * Array of readers for reading the randomness values from the different files
   */
  private BufferedReader[]    randomnessReaders;

  /**
   * Array of readers for reading the commit values from the different files
   */
  private BufferedReader[]    commitReaders;

  /**
   * Array of Cipher objects for performing the decryption on the contents of the different randomness files
   */
  private Cipher[]            aesCiphers;

  /**
   * String of the currentSerialNo we are generating
   */
  private String              currentSerialNo = null;

  /**
   * Utility class for finding all the Randomness files
   */
  private RandomnessFiles     randFiles;

  /**
   * Construct the CombinedRandomness object, with the ElGamal KeyPair and the RandomnessFiles class that has found all the
   * randomness files available. It is essential to call closeAllFiles() once finished to close the open readers.
   * 
   * @param keyPair
   *          JSONObject containing ElGamal key pair
   * @param randomnessFiles
   *          RandomnessFiles object that contains references to the different randomness files
   * @param aesAlg
   *          AES Algorithm to use
   * 
   * 
   * @throws FileNotFoundException
   * @throws CombinedRandomnessException
   */
  public CombinedRandomness(JSONObject keyPair, RandomnessFiles randomnessFiles, String aesAlg) throws CombinedRandomnessException {
    super();

    try {
      logger.info("Creating CombinedRandomness");
      this.randFiles = randomnessFiles;

      // Prepare arrays
      this.randomnessReaders = new BufferedReader[this.randFiles.getRandomnessFiles().size()];
      this.commitReaders = new BufferedReader[this.randFiles.getCommitFiles().size()];
      this.aesCiphers = new Cipher[this.randFiles.getAESKeys().size()];

      // Step through randomness files preparing readers and cipher objects
      for (int i = 0; i < this.randFiles.getRandomnessFiles().size(); i++) {
        logger.info("Reading randomness from {}", this.randFiles.getRandomnessFiles().get(i));
        this.randomnessReaders[i] = new BufferedReader(new FileReader(this.randFiles.getRandomnessFiles().get(i)));
        this.commitReaders[i] = new BufferedReader(new FileReader(this.randFiles.getCommitFiles().get(i)));

        // Load aes key from file
        logger.info("Reading AESKey from {}", this.randFiles.getAESKeys().get(i));
        JSONObject aesKeyObj = IOUtils.readJSONObjectFromFile(this.randFiles.getAESKeys().get(i));
        this.aesCiphers[i] = CryptoUtils.getAESDecryptionCipherFromEncKey(aesKeyObj, keyPair, aesAlg);
      }
    }
    catch (IOException e) {
      throw new CombinedRandomnessException("Exception occured when preparing CombinedRandomness", e);
    }
    catch (CryptoIOException e) {
      throw new CombinedRandomnessException("Exception occured when preparing CombinedRandomness", e);
    }
    catch (JSONIOException e) {
      throw new CombinedRandomnessException("Exception occured when preparing CombinedRandomness", e);
    }
  }

  /**
   * Closes any open readers - this must be called having finished performing the searches for a serial number
   */
  public void closeAllFiles() {
    logger.info("Closing all randomness files");
    for (BufferedReader randReader : this.randomnessReaders) {
      if (randReader != null) {
        try {
          randReader.close();
        }
        catch (IOException e1) {
          logger.error("Exception whilst closing file", e1);
        }
      }
      else {
        logger.error("Trying to close a null file");
      }
    }
    for (BufferedReader commReader : this.commitReaders) {
      if (commReader != null) {
        try {
          commReader.close();
        }
        catch (IOException e1) {
          logger.error("Exception whilst closing file", e1);
        }
      }
      else {
        logger.error("Trying to close a null file");
      }
    }
  }

  /**
   * Finds a particular serial number entry from each of the randomness and commit files. This is required during an audit where we
   * are only interested in a selected set of serial numbers and their corresponding randomness values.
   * 
   * The search itself is designed to be as efficient as possible, however, it will only be optimal for multiple searches if the
   * searches are conducted on an ordered list of serial numbers, since the underlying data is an ordered list of serial number. The
   * fundamental approach is for the search to continue from the last point in the file looking for the serial number. If it finds
   * it the search stops and the data is returned. The next search will start from where it left off. If it reaches the end of the
   * file it will close and reopen the files and start from the beginning and continue the search up to the end of the file again.
   * This is slightly inefficient but saves us from storing locations in a file of unknown length. If it still hasn't found the
   * serialNo it throws and exception
   * 
   * @param serialNo
   *          String of the serialNo to find
   * @return JSONArray containing the audit data from each randomness entry or null if not found
   * @throws SerialNumberNotFoundException
   * @throws FindSerialNoException
   */
  public JSONArray findSerial(String serialNo) throws SerialNumberNotFoundException, FindSerialNoException {
    try {
      // Reset the list of randomness rows
      this.randomness.clear();
      this.currentSerialNo = null;

      logger.info("Searching for serial number {}", serialNo);

      // Step through each source of randomness
      for (int counter = 0; counter < this.randomnessReaders.length; counter++) {
        // Used to stop the search if we find the serialNo
        boolean searchInProgress = true;

        // Set to true when we have reached the end of the file in this search
        boolean haveReset = false;

        while (searchInProgress) {
          // Read the next line
          String randLine = this.randomnessReaders[counter].readLine();
          String commitLine = this.commitReaders[counter].readLine();

          // Check neither is null (they should be in sync, but just in case we check both)
          if (randLine == null || commitLine == null) {
            // We have reached the end of the file. Close and re-open the readers
            this.randomnessReaders[counter].close();
            this.commitReaders[counter].close();
            this.randomnessReaders[counter] = new BufferedReader(new FileReader(this.randFiles.getRandomnessFiles().get(counter)));
            this.commitReaders[counter] = new BufferedReader(new FileReader(this.randFiles.getCommitFiles().get(counter)));

            // If we have already reset it means we cannot find the serial number
            if (haveReset) {
              logger.error("Cannot find serialNo: {} in randomness files", serialNo);
              throw new SerialNumberNotFoundException("Cannot find serialNo: " + serialNo + " in randomness files");
            }

            // Set the haveReset
            haveReset = true;

            // Read the first line of the file
            randLine = this.randomnessReaders[counter].readLine();
            commitLine = this.commitReaders[counter].readLine();
          }

          // We haven't reached the end, so load the data into a RandomnessRow object
          RandomnessRow row = new RandomnessRow(randLine, this.aesCiphers[counter], commitLine);

          // Check if serial numbers are equal
          if (row.getSerialNo().equals(serialNo)) {
            // If they are we set the peerID to we can track where it came from
            row.setPeerID(this.randFiles.getPeerIDS().get(counter));
            this.randomness.add(row);

            // stop the search
            break;
          }
        }
      }

      // Combine the found value into a single JSONArray
      JSONArray peerRandomness = new JSONArray();
      for (RandomnessRow row : this.randomness) {
        peerRandomness.put(row.getAuditData());
      }

      // Return the combined set of audit values
      return peerRandomness;
    }
    catch (IOException e) {
      throw new FindSerialNoException("Exception occured when searching for a serialNo:" + serialNo, e);
    }
    catch (JSONException e) {
      throw new FindSerialNoException("Exception occured when searching for a serialNo:" + serialNo, e);
    }
    catch (RandomnessCommitmentException e) {
      throw new FindSerialNoException("Exception occured when searching for a serialNo:" + serialNo, e);
    }
    catch (EndOfFileException e) {
      throw new FindSerialNoException("Exception occured when searching for a serialNo:" + serialNo, e);
    }
  }

  /**
   * Gets the current serial number.
   * 
   * @return String containing the current serial number
   */
  public String getCurrentSerialNo() {
    return this.currentSerialNo;
  }

  /**
   * Combines the randomness values by hashing them together and returns the output of the hash.
   * 
   * @return byte[] containing combined random data
   * @throws RandomnessCommitmentException
   * @throws CombinedRandomnessException
   */
  public byte[] getNextCombinedValue() throws RandomnessCommitmentException, CombinedRandomnessException {
    try {
      // Create hash
      MessageDigest md = MessageDigest.getInstance("SHA256", CryptoUtils.BC_PROV);

      // Add values to it
      for (RandomnessRow row : this.randomness) {
        md.update(row.getNextValue());
      }

      return md.digest();
    }
    catch (NoSuchAlgorithmException e) {
      throw new CombinedRandomnessException("Exception whilst combining random values", e);
    }
    
  }

  /**
   * Moves to the next row in the files of randomness values. Also checks that the serial number is consistent.
   * 
   * @throws SerialNumberException
   * @throws CombinedRandomnessException
   * @throws EndOfFileException 
   */
  public void nextRow() throws SerialNumberException, CombinedRandomnessException, EndOfFileException {
    try {
      // reset the current fields
      this.randomness.clear();
      this.currentSerialNo = null;
      int counter = 0;

      // Step through each reader
      for (BufferedReader randReader : this.randomnessReaders) {
        // Create a new row to represent the next line of data
        RandomnessRow row = new RandomnessRow(randReader.readLine(), this.aesCiphers[counter],
            this.commitReaders[counter].readLine());
        this.randomness.add(row);

        // Check that the serial numbers are all the same and not null
        if (this.currentSerialNo == null) {
          this.currentSerialNo = row.getSerialNo();
          if (this.currentSerialNo == null) {
            throw new SerialNumberException("A serial number in a randomness file is null");
          }
        }
        else {
          if (!row.getSerialNo().equals(this.currentSerialNo)) {
            throw new SerialNumberException(
                "The serial numbers are not equal in the randomness files - indicates inconsistency in files");
          }
        }
        counter++;
      }
    }
    catch (IOException e) {
      throw new CombinedRandomnessException("An exception has occured whilst reading the next randomness values", e);
    }
    catch (JSONException e) {
      throw new CombinedRandomnessException("An exception has occured whilst reading the next randomness values", e);
    }
    
  }
}
