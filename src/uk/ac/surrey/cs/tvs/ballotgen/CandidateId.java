/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * Data class to store a candidateID for easy access during processing.
 * 
 * @author Chris Culnane
 * 
 */
public class CandidateId {

  /**
   * JSONObject containing the encrypted candidate ID
   */
  private JSONObject candIdObject;

  /**
   * ECGroup that contains utility methods for performing EC crypto
   */
  private ECUtils    group = new ECUtils();

  /**
   * Create object with JSONObject.
   * 
   * @param candId
   *          JSONObject containing encrypted candidate ID
   * 
   */
  public CandidateId(JSONObject candId) {
    super();

    this.candIdObject = candId;
  }

  /**
   * Creates a new CandidateCipherText object referring to the relevant ECGroup and candidateID values. This is preparing for
   * re-encryption.
   * 
   * @return CandidateCipherText containing candidate Id
   * @throws JSONException
   */
  public CandidateCipherText getNewCipherText() throws JSONException {
    return new CandidateCipherText(this.group, this.candIdObject);
  }
}
