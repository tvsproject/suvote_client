/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipOutputStream;

import javax.net.ssl.SSLSocketFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenAuditException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.CombinedRandomnessException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.EndOfFileException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.FindSerialNoException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberException;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException;
import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.BallotGenConfig;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.BallotAuditCommitMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Class to perform a ballot generation audit. All this class does it extract out the information necessary for an audit, packages
 * it up and submits it to the WBB. The checking of the data should be performed independently.
 * 
 * @author Chris Culnane
 * 
 */
public class BallotGenAudit {

  /**
   * JSONObject containing the ElGamal keypair for this device
   */
  private JSONObject             myKeyPair;

  /**
   * RandomnessFiles object that contains references to all the randomness files from the different sources
   */
  private RandomnessFiles        randomnessFiles;

  /**
   * Logger
   */
  private static final Logger    logger            = LoggerFactory.getLogger(BallotGenAudit.class);

  /**
   * SSLSocketFactory used for connecting with MBB
   */
  private SSLSocketFactory       factory           = null;

  /**
   * String array of accepted cipher suites
   */
  private String[]               cipherSuites      = null;

  /**
   * ConfigFile representing the client config
   */
  private ConfigFile             clientConfig;

  /**
   * Current progress
   */
  private float                  progress          = 0f;

  /**
   * Last integer progress amount, used to increment an integer progress listener with a float based progress value
   */
  private int                    lastProgress      = 0;

  /**
   * Holds a reference to the ballot generation config - needed for removing audited ballots
   */
  private BallotGenConf          ballotGenConf;
  /**
   * List of ProgressListeners
   */
  private List<ProgressListener> progressListeners = new ArrayList<ProgressListener>();

  /**
   * Constructs a BallotGenAudit object to perform the audit.
   * 
   * @param ballotGenConfFile
   *          BallotGenConf object holding the ballot generation config file
   * @param myKeyPairFile
   *          String path to JSONObject with ElGamal key pair
   * @param inputDir
   *          String path to top-level randomness directory
   * @param clientConf
   *          ConfigFile for the Client configuration
   * @throws BallotGenAuditException
   */
  public BallotGenAudit(BallotGenConf ballotGenConfFile, String myKeyPairFile, String inputDir, ConfigFile clientConf)
      throws BallotGenAuditException {
    super();

    try {
      this.randomnessFiles = new RandomnessFiles(inputDir);
      this.randomnessFiles.findRandomnessFiles();
      this.myKeyPair = IOUtils.readJSONObjectFromFile(myKeyPairFile);
      this.clientConfig = clientConf;
      this.ballotGenConf = ballotGenConfFile;
    }
    catch (JSONIOException e) {
      throw new BallotGenAuditException("Exception when constructing BallotGenAudit", e);
    }
    catch (JSONException e) {
      throw new BallotGenAuditException("Exception when constructing BallotGenAudit", e);
    }
  }

  /**
   * Adds a ProgressListener to this class
   * 
   * @param listener
   *          ProgressListener to add
   */
  public void addProgressListener(ProgressListener listener) {
    this.progressListeners.add(listener);
  }

  /**
   * Performs the actual work associated with an audit, extracting randomness and combining it into a single file.
   * 
   * @param auditFile
   *          String path to the list of serial numbers to audit
   * @param outputFile
   *          String path to the output file for the audit
   * @throws BallotGenAuditException
   */
  public void doAudit(String auditFile, String outputFile) throws BallotGenAuditException {
    try {
      logger.info("About to perform audit using {} saving to {}", auditFile, outputFile);

      // Construct a combinedRandomness object
      CombinedRandomness combinedRand = new CombinedRandomness(this.myKeyPair, this.randomnessFiles,
          this.clientConfig.getStringParameter(ClientConfig.AES_ALGORITHM));
      combinedRand.nextRow();

      // Create new output for the audit
      BufferedWriter auditOutput = new BufferedWriter(new FileWriter(outputFile));
      try {
        // Read the audit file
        JSONObject auditJSON = IOUtils.readJSONObjectFromFile(auditFile);
        // List of all ballots
        JSONArray ballots = IOUtils.readJSONArrayFromFile(this.ballotGenConf.getStringParameter(BallotGenConfig.BALLOT_LIST));

        // Gets the ballots to audit
        JSONArray ballotsToAudit = auditJSON.getJSONArray(ClientConstants.AuditFile.AUDIT_SERIALS);

        float increment = 80f / ballotsToAudit.length();
        String ballotOutputFolder = clientConfig.getStringParameter(ClientConfig.BALLOT_OUTPUT_FOLDER);
        // Step through the serial numbers
        for (int ballotCounter = 0; ballotCounter < ballotsToAudit.length(); ballotCounter++) {
          logger.info("Ballot {} marked as ballot gen audit, will remove",ballotsToAudit.getString(ballotCounter));
          // Find the serial number and get the audit data
          JSONArray auditData = combinedRand.findSerial(ballotsToAudit.getString(ballotCounter));
          auditOutput.write(auditData.toString());
          auditOutput.newLine();
          // Remove ballot from list of ballots
          String ballotFile = ballotOutputFolder + File.separator
              + IOUtils.getFileNameFromSerialNo(ballotsToAudit.getString(ballotCounter))
              + MessageFields.Ballot.BALLOT_FILE_EXTENSION;
          boolean removedBallot = false;
          for (int i = 0; i < ballots.length(); i++) {
            if (ballots.getString(i).equals(ballotFile)) {

              ballots.remove(i);
              File delBallot = new File(ballotFile);
              logger.info("Will delete {}",delBallot.getAbsolutePath());
              IOUtils.checkAndDeleteFile(delBallot);
              
              removedBallot = true;
              break;
            }
          }
          if (!removedBallot) {
            throw new BallotGenAuditException("Could not delete ballot gen audit ballot from list of ballots");
          }

          this.incrementProgress(increment);
        }
        IOUtils.writeJSONToFile(ballots, this.ballotGenConf.getStringParameter(BallotGenConfig.BALLOT_LIST));
        logger.info("Finished getting Audit data, closing files");
      }
      finally {
        // Close the files
        try {
          auditOutput.close();
        }
        finally {
          combinedRand.closeAllFiles();
        }
      }
    }
    catch (IOException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
    catch (CombinedRandomnessException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
    catch (SerialNumberException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
    catch (JSONIOException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
    catch (JSONException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
    catch (SerialNumberNotFoundException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
    catch (FindSerialNoException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
    catch (EndOfFileException e) {
      throw new BallotGenAuditException("An exception has occured whilst conducting a BallotGen audit", e);
    }
  }

  /**
   * Increments the progress with the float increment. This will only fire an updateProgressListeners call if the integer progress
   * will increase following the float increment.
   * 
   * @param increment
   *          float increment amount to add to the total
   */
  public void incrementProgress(float increment) {
    // Update progress total
    this.progress = this.progress + increment;

    // Round the value to an integer
    int tProgress = Math.round(this.progress);

    // Check if that results in a change to the integer progress, notify listeners if it does, if not do nothing
    if (tProgress != this.lastProgress) {
      this.lastProgress = tProgress;
      this.updateProgressListeners(this.lastProgress);
    }
  }

  /**
   * Removes a ProgressListener from this class
   * 
   * @param listener
   *          ProgressListener to remove
   */
  public void removeProgressListener(ProgressListener listener) {
    this.progressListeners.remove(listener);
  }

  /**
   * Sets the SSLSocketFactory and CipherSuites to use. If not set the client will revert to standard sockets instead of SSL.
   * However, the MBB is likely to demand SSL only.
   * 
   * @param factory
   *          SSLSocketFactory to create sockets from
   * @param cipherSuite
   *          String array of acceptable cipher suites
   */
  public void setSSLFactoryAndCipherSuite(SSLSocketFactory factory, String[] cipherSuite) {
    logger.info("Setting SSLSocketFactory CipherSuites");
    this.factory = factory;
    this.cipherSuites = cipherSuite;
  }

  /**
   * Submits the output from an audit to the MBB. Again, this does not check the contents for validity, neither does the MBB, this
   * is just a commitment.
   * 
   * @param filename
   *          String path to audit file to send
   * @param submitOutput
   *          String path to zip file to create when sending to MBB
   * @return JSONObject of the message sent to the MBB and the returned signatures
   * @throws BallotGenAuditException
   */
  public JSONObject submitAuditToWBB(String filename, String submitOutput) throws BallotGenAuditException {
    try {
      logger.info("About to submit {} to the MBB. Creating file {}", filename, submitOutput);
      File wbbSubFile = new File(submitOutput);
      
      // Create zip file and add submission to it
      ZipOutputStream zos = null;
      MessageDigest md = MessageDigest.getInstance(ClientConstants.DEFAULT_MESSAGE_DIGEST);
      try {
        zos = new ZipOutputStream(new FileOutputStream(wbbSubFile));
        ZipUtil.addFileToZip(new File(filename),BallotAuditCommitMessage.AUDIT_DATA_ZIP_ENTRY, md, zos);
        ZipUtil.addFileToZip(new File(this.clientConfig.getStringParameter(ConfigFiles.ClientConfig.CIPHER_SUBMISSION_RESPONSE)), BallotAuditCommitMessage.SUBMISSION_RESPONSE_ZIP_ENTRY,md, zos);
      }
      finally {
        zos.close();
      }

      JSONObject message = BallotGenUtils.createMBBMessage(this.clientConfig, MessageTypes.BALLOT_AUDIT_COMMIT, md,
          wbbSubFile.length());

      MBBConfig mbbConf = new MBBConfig(this.clientConfig.getStringParameter(ClientConfig.MBB_CONF_FILE));
      JSONArray sigArray = MBB.sendMessage(mbbConf, message.toString() + MBB.FILE_SUB_SEPARATOR + wbbSubFile.getAbsolutePath(),
          this.factory, this.cipherSuites);
      String commitTime = MBB.getThresholdFieldValue(sigArray, MessageFields.PeerResponse.COMMIT_TIME,
          this.clientConfig.getIntParameter(ClientConfig.THRESHOLD));

      logger.info("Received response, will check signature");
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, CryptoUtils.loadTVSKeyStore(this.clientConfig
          .getStringParameter(ClientConfig.SYSTEM_CERTIFICATE_STORE)));
      tvsSig.update(message.getString(FileMessage.ID));
      tvsSig.update(message.getString(FileMessage.DIGEST));
      tvsSig.update(message.getString(FileMessage.SENDER_ID));
      tvsSig.update(commitTime);

      JSONObject response = tvsSig.verifyAndCombine(sigArray, SystemConstants.SIGNING_KEY_SK2_SUFFIX,
          this.clientConfig.getIntParameter(ClientConfig.THRESHOLD), mbbConf.getPeers().size(), true, true);

      logger.info("Signature on response verified");
      // Check whether we received valid threshold of responses
      if (response.has(TVSSignature.COMBINED)) {

        // If BLS combine into a single object prepare the output object prepare a fiatShamir value
        message.put(ClientConstants.BallotAuditCommitResponse.WBB_SIG, response.get(TVSSignature.COMBINED));
        return message;
      }
      else {
        logger.error("Failed to submit to MBB: did not receive a threshold of valid responses");
        throw new MBBCommunicationException(
            "Failed to submit to MBB: did not receive a threshold of valid responses, see log for further details");
      }
    }
    catch (IOException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
    catch (CryptoIOException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
    catch (TVSSignatureException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
    catch (JSONException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
    catch (JSONIOException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
    catch (MBBCommunicationException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
    catch (ConsensusException e) {
      throw new BallotGenAuditException("Inconsistencies in response meant no threshold response could be found", e);
    }
    catch (KeyStoreException e) {
      throw new BallotGenAuditException("Exception whilst sending audit to MBB", e);
    }
  }

  /**
   * Called to update any ProgressListeners with a new value. The value they receive is the parameter
   * 
   * @param value
   *          to pass to the ProgressListeners
   */
  private void updateProgressListeners(int value) {
    for (ProgressListener pl : this.progressListeners) {
      pl.updateProgress(value);
    }
  }
}
