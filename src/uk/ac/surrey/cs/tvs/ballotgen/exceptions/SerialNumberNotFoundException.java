/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

/**
 * Exception used when a serial number is not found in the randomness files.
 * 
 * @author Chris Culnane
 * 
 */
public class SerialNumberNotFoundException extends Exception {

  /**
   * For serialisation.
   */
  private static final long serialVersionUID = -2777944399687642501L;

  /**
   * Default constructor.
   */
  public SerialNumberNotFoundException() {
    super();
  }

  /**
   * Constructor which allows the message to be defined.
   * 
   * @param message
   *          The exception message.
   */
  public SerialNumberNotFoundException(String message) {
    super(message);
  }

  /**
   * Constructor which allows the message and cause to be defined.
   * 
   * @param message
   *          The exception message.
   * @param cause
   *          The cause of the exception.
   */
  public SerialNumberNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructor which allows the message and cause to be defined and other optional parameters.
   * 
   * @param message
   *          The exception message.
   * @param cause
   *          The cause of the exception.
   * @param enableSuppression
   *          Whether or not suppression is enabled or disabled.
   * @param writableStackTrace
   *          Whether or not the stack trace should be writable.
   */
  public SerialNumberNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  /**
   * Constructor which allows the cause to be defined.
   * 
   * @param cause
   *          The cause of the exception.
   */
  public SerialNumberNotFoundException(Throwable cause) {
    super(cause);
  }

}
