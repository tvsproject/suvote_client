#!/bin/bash
cd ..
export PEERNAME="VPS"
openbrowserIn3Seconds(){
echo "Will open browser in 3 seconds"
sleep 3
if which xdg-open > /dev/null
then
  xdg-open http://127.0.0.1:8081/
elif which gnome-open > /dev/null
then
  gnome-open http://127.0.0.1:8081/
fi
}
openbrowserIn3Seconds &
java -Dlogback.configurationFile=./release_demo/logback-demo.xml -jar ./release/vecclient.jar ./release_demo/TestDeviceOne/client.conf 


