/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.PretendMBB;
import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.ballotgen.BallotGenMix;
import uk.ac.surrey.cs.tvs.ballotgen.init.CandidateIDs;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * The class <code>MBBPODProxyServletTest</code> contains tests for the class <code>{@link MBBPODProxyServlet}</code>.
 */
public class MBBPODProxyServletTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate candidate ids.
    CandidateIDs.create(TestParameters.CANDIDATES, TestParameters.PUBLIC_KEY_FILE, TestParameters.OUTPUT_PLAIN_TEXT_FILE,
        TestParameters.OUTPUT_ENCRYPTED_FILE);
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the MBBPODProxyServlet(String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBPODProxyServlet_1() throws Exception {
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet result = new MBBPODProxyServlet(client, TestParameters.DISTRICT_CONFIG_FILE);
    assertNotNull(result);
  }

  /**
   * Run the MBBPODProxyServlet(String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONIOException.class)
  public void testMBBPODProxyServlet_2() throws Exception {
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet result = new MBBPODProxyServlet(client, "rubbish");
    assertNull(result);
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_1() throws Exception {
    // Create an invalid test message.
    JSONObject request = new JSONObject();

    request.put(MessageFields.TYPE, MessageTypes.AUDIT);

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet fixture = new MBBPODProxyServlet(client, TestParameters.DISTRICT_CONFIG_FILE);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", request.toString());

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_PLAINTEXT, result.getMimeType());
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_2() throws Exception {
    // Generate ballots.
    BallotGenMix ballots = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate the ballots.
    ballots.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Create a test message.
    JSONObject request = new JSONObject();

    request.put(MessageFields.TYPE, MessageTypes.AUDIT);
    request.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    request.put(JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    request.put(JSONWBBMessage.SERIAL_SIG, TestParameters.signData(TestParameters.DISTRICT,0));

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet fixture = new MBBPODProxyServlet(client, TestParameters.DISTRICT_CONFIG_FILE);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", request.toString());

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false);

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.OK, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_JSON, result.getMimeType());

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test the sent message.
    ByteArrayInputStream data = (ByteArrayInputStream) result.getData();
    data.reset();

    byte[] dataBytes = new byte[data.available()];
    data.read(dataBytes);

    JSONObject response = new JSONObject(new String(dataBytes));
    
    assertTrue(response.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(response.has(MessageFields.TYPE));
    assertTrue(response.has(MessageFields.JSONWBBMessage.SENDER_ID));
    assertTrue(response.has(MessageFields.JSONWBBMessage.SENDER_SIG));

    assertTrue(response.get(MessageFields.JSONWBBMessage.ID).toString().startsWith(TestParameters.DEVICE_NAME));
    assertEquals(request.get(MessageFields.TYPE), response.get(MessageFields.TYPE));
    assertEquals(TestParameters.DEVICE_NAME, response.get(MessageFields.JSONWBBMessage.SENDER_ID));
    assertTrue(response.get(MessageFields.JSONWBBMessage.SENDER_SIG).toString().length() > 0);

    // Test the message received by the MBB.
    for (PretendMBB pretendMBB : pretendMBBs) {
      List<JSONObject> messages = pretendMBB.getMessages();
      assertEquals(1, messages.size());

      for (JSONObject message : messages) {
        assertTrue(message.has(MessageFields.JSONWBBMessage.ID));
        assertTrue(message.has(MessageFields.TYPE));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_SIG));

        assertTrue(message.get(MessageFields.JSONWBBMessage.ID).toString().startsWith(TestParameters.DEVICE_NAME));
        assertEquals(request.get(MessageFields.TYPE), message.get(MessageFields.TYPE));
        assertEquals(TestParameters.DEVICE_NAME, message.get(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.get(MessageFields.JSONWBBMessage.SENDER_SIG).toString().length() > 0);
      }
    }
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_3() throws Exception {
    // Generate ballots.
    BallotGenMix ballots = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate the ballots.
    ballots.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Create a test message.
    JSONObject request = new JSONObject();

    request.put(MessageFields.TYPE, MessageTypes.CANCEL);
    request.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    request.put(JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    request.put(JSONWBBMessage.SERIAL_SIG, TestParameters.signData(TestParameters.DISTRICT,0));
    request.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.DISTRICT);
    request.put(MessageFields.CancelMessage.AUTH_SIG, TestParameters.signData(TestParameters.DISTRICT,0));

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet fixture = new MBBPODProxyServlet(client, TestParameters.DISTRICT_CONFIG_FILE);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", request.toString());

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false);

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.OK, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_JSON, result.getMimeType());

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test the sent message.
    ByteArrayInputStream data = (ByteArrayInputStream) result.getData();
    data.reset();

    byte[] dataBytes = new byte[data.available()];
    data.read(dataBytes);

    JSONObject response = new JSONObject(new String(dataBytes));

    assertTrue(response.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(response.has(MessageFields.TYPE));
    assertTrue(response.has(MessageFields.JSONWBBMessage.SENDER_ID));
    assertTrue(response.has(MessageFields.JSONWBBMessage.SENDER_SIG));

    assertTrue(response.get(MessageFields.JSONWBBMessage.ID).toString().startsWith(TestParameters.DEVICE_NAME));
    assertEquals(request.get(MessageFields.TYPE), response.get(MessageFields.TYPE));
    assertEquals(TestParameters.DEVICE_NAME, response.get(MessageFields.JSONWBBMessage.SENDER_ID));
    assertTrue(response.get(MessageFields.JSONWBBMessage.SENDER_SIG).toString().length() > 0);

    // Test the message received by the MBB.
    for (PretendMBB pretendMBB : pretendMBBs) {
      List<JSONObject> messages = pretendMBB.getMessages();
      assertEquals(1, messages.size());

      for (JSONObject message : messages) {
        assertTrue(message.has(MessageFields.JSONWBBMessage.ID));
        assertTrue(message.has(MessageFields.TYPE));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_SIG));

        assertTrue(message.get(MessageFields.JSONWBBMessage.ID).toString().startsWith(TestParameters.DEVICE_NAME));
        assertEquals(request.get(MessageFields.TYPE), message.get(MessageFields.TYPE));
        assertEquals(TestParameters.DEVICE_NAME, message.get(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.get(MessageFields.JSONWBBMessage.SENDER_SIG).toString().length() > 0);
      }
    }
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_4() throws Exception {
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet fixture = new MBBPODProxyServlet(client, TestParameters.DISTRICT_CONFIG_FILE);

    Map<String, String> params = new Hashtable<String, String>();

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_PLAINTEXT, result.getMimeType());
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_5() throws Exception {
    // Generate ballots.
    BallotGenMix ballots = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate the ballots.
    ballots.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Create a test message.
    JSONObject request = new JSONObject();

    request.put(MessageFields.TYPE, MessageTypes.AUDIT);
    request.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    request.put(JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    request.put(JSONWBBMessage.SERIAL_SIG, TestParameters.signData(TestParameters.DISTRICT,0));

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet fixture = new MBBPODProxyServlet(client, TestParameters.DISTRICT_CONFIG_FILE);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", request.toString());

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.OK, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_JSON, result.getMimeType());
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_6() throws Exception {
    // Generate ballots.
    BallotGenMix ballots = new BallotGenMix(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate the ballots.
    ballots.generateBallots(TestParameters.OUTPUT_BALLOT_FOLDER, TestParameters.OUTPUT_BALLOT_CIPHERS_FILE);

    // Create a test message.
    JSONObject request = new JSONObject();

    request.put(MessageFields.TYPE, MessageTypes.CANCEL);
    request.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    request.put(JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    request.put(JSONWBBMessage.SERIAL_SIG, TestParameters.signData(TestParameters.DISTRICT,0));
    request.put(MessageFields.CancelMessage.AUTH_ID, TestParameters.DISTRICT);
    request.put(MessageFields.CancelMessage.AUTH_SIG, TestParameters.signData(TestParameters.DISTRICT,0));

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBPODProxyServlet fixture = new MBBPODProxyServlet(client, TestParameters.DISTRICT_CONFIG_FILE);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", request.toString());

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.OK, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_JSON, result.getMimeType());
  }
}