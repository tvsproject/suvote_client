/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.web;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.PretendMBB;
import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.client.Client;
import uk.ac.surrey.cs.tvs.comms.http.VVoteWebServer;
import uk.ac.surrey.cs.tvs.comms.http.thirdparty.VVoteNanoHTTPD;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.VoteMessage;
import fi.iki.elonen.NanoHTTPD.Response;

/**
 * The class <code>MBBProxyServletTest</code> contains tests for the class <code>{@link MBBProxyServlet}</code>.
 */
public class MBBProxyServletTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the MBBProxyServlet(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testMBBProxyServlet_1() throws Exception {
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBProxyServlet result = new MBBProxyServlet(client);
    assertNotNull(result);
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_1() throws Exception {
    // Create an invalid test vote.
    JSONObject vote = new JSONObject();

    vote.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    vote.put(MessageFields.TYPE, "vote");

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBProxyServlet fixture = new MBBProxyServlet(client);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", vote.toString());

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_PLAINTEXT, result.getMimeType());
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_2() throws Exception {
    // Create a test vote.
    JSONObject vote = new JSONObject();

    vote.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    vote.put(MessageFields.TYPE, "vote");
    vote.put(MessageFields.JSONWBBMessage.SERIAL_SIG, TestParameters.BOOTH_SIGNATURE);
    vote.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    vote.put(VoteMessage.START_EVM_SIG, TestParameters.BOOTH_SIGNATURE);

    JSONArray raceArray = new JSONArray();

    JSONObject la = new JSONObject();
    la.put(VoteMessage.RACE_ID, "la");
    la.put(VoteMessage.PREFERENCES, new JSONArray("[\"1\",\" \",\" \"]"));
    raceArray.put(la);

    JSONObject lcATL = new JSONObject();
    lcATL.put(VoteMessage.RACE_ID, "lc_atl");
    lcATL.put(VoteMessage.PREFERENCES, new JSONArray("[\"1\",\" \",\" \"]"));
    raceArray.put(lcATL);

    JSONObject lcBTL = new JSONObject();
    lcBTL.put(VoteMessage.RACE_ID, "lc_btl");
    lcBTL.put(VoteMessage.PREFERENCES, new JSONArray("[\" \",\" \",\" \"]"));
    raceArray.put(lcBTL);

    vote.put(VoteMessage.RACES, raceArray);

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBProxyServlet fixture = new MBBProxyServlet(client);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", vote.toString());

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false);

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.OK, result.getStatus());
    assertEquals(VVoteWebServer.MIME_JSON, result.getMimeType());

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test the sent message.
    ByteArrayInputStream data = (ByteArrayInputStream) result.getData();
    data.reset();

    byte[] dataBytes = new byte[data.available()];
    data.read(dataBytes);

    JSONObject response = new JSONObject(new String(dataBytes));

    assertTrue(response.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(response.has(MessageFields.JSONWBBMessage.DISTRICT));
    assertTrue(response.has(VoteMessage.RACES));

    assertEquals(vote.get(MessageFields.JSONWBBMessage.ID), response.get(MessageFields.JSONWBBMessage.ID));
    assertEquals(vote.get(MessageFields.JSONWBBMessage.DISTRICT), response.get(MessageFields.JSONWBBMessage.DISTRICT));
    assertEquals(vote.get(VoteMessage.RACES).toString(), response.get(VoteMessage.RACES).toString());

    // Test the message received by the MBB.
    for (PretendMBB pretendMBB : pretendMBBs) {
      List<JSONObject> messages = pretendMBB.getMessages();
      assertEquals(1, messages.size());

      for (JSONObject message : messages) {
        assertTrue(message.has(MessageFields.JSONWBBMessage.ID));
        assertTrue(message.has(MessageFields.TYPE));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_SIG));
        assertTrue(message.has(MessageFields.JSONWBBMessage.DISTRICT));

        assertEquals(vote.get(MessageFields.JSONWBBMessage.ID), message.get(MessageFields.JSONWBBMessage.ID));
        assertEquals(vote.get(MessageFields.TYPE), message.get(MessageFields.TYPE));
        assertEquals(TestParameters.DEVICE_NAME, message.get(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.get(MessageFields.JSONWBBMessage.SENDER_SIG).toString().length() > 0);
        assertEquals(vote.get(MessageFields.JSONWBBMessage.DISTRICT), message.get(MessageFields.JSONWBBMessage.DISTRICT));
      }
    }
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_3() throws Exception {
    // Create a test vote.
    JSONObject startEVM = new JSONObject();

    startEVM.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    startEVM.put(MessageFields.TYPE, "startevm");
    startEVM.put(MessageFields.JSONWBBMessage.SERIAL_SIG, TestParameters.BOOTH_SIGNATURE);
    startEVM.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBProxyServlet fixture = new MBBProxyServlet(client);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", startEVM.toString());

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB(false);

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.OK, result.getStatus());
    assertEquals(VVoteWebServer.MIME_JSON, result.getMimeType());

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Test the sent message.
    ByteArrayInputStream data = (ByteArrayInputStream) result.getData();
    data.reset();

    byte[] dataBytes = new byte[data.available()];
    data.read(dataBytes);

    JSONObject response = new JSONObject(new String(dataBytes));

    assertTrue(response.has(MessageFields.JSONWBBMessage.ID));
    assertTrue(response.has(MessageFields.JSONWBBMessage.DISTRICT));

    assertEquals(startEVM.get(MessageFields.JSONWBBMessage.ID), response.get(MessageFields.JSONWBBMessage.ID));
    assertEquals(startEVM.get(MessageFields.JSONWBBMessage.DISTRICT), response.get(MessageFields.JSONWBBMessage.DISTRICT));

    // Test the message received by the MBB.
    for (PretendMBB pretendMBB : pretendMBBs) {
      List<JSONObject> messages = pretendMBB.getMessages();
      assertEquals(1, messages.size());

      for (JSONObject message : messages) {
        assertTrue(message.has(MessageFields.JSONWBBMessage.ID));
        assertTrue(message.has(MessageFields.TYPE));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.has(MessageFields.JSONWBBMessage.SENDER_SIG));
        assertTrue(message.has(MessageFields.JSONWBBMessage.DISTRICT));

        assertEquals(startEVM.get(MessageFields.JSONWBBMessage.ID), message.get(MessageFields.JSONWBBMessage.ID));
        assertEquals(startEVM.get(MessageFields.TYPE), message.get(MessageFields.TYPE));
        assertEquals(TestParameters.DEVICE_NAME, message.get(MessageFields.JSONWBBMessage.SENDER_ID));
        assertTrue(message.get(MessageFields.JSONWBBMessage.SENDER_SIG).toString().length() > 0);
        assertEquals(startEVM.get(MessageFields.JSONWBBMessage.DISTRICT), message.get(MessageFields.JSONWBBMessage.DISTRICT));
      }
    }
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_4() throws Exception {
    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBProxyServlet fixture = new MBBProxyServlet(client);

    Map<String, String> params = new Hashtable<String, String>();

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.BAD_REQUEST, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_PLAINTEXT, result.getMimeType());
  }

  /**
   * Run the uk.ac.surrey.cs.tvs.comms.http.NanoHTTPD.Response
   * runServlet(String,Method,Map<String,String>,Map<String,String>,Map<String,String>,File) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRunServlet_5() throws Exception {
    // Create a test vote.
    JSONObject vote = new JSONObject();

    vote.put(MessageFields.JSONWBBMessage.ID, TestParameters.SERIAL_NUMBERS[0]);
    vote.put(MessageFields.TYPE, "vote");
    vote.put(MessageFields.JSONWBBMessage.SERIAL_SIG, TestParameters.BOOTH_SIGNATURE);
    vote.put(MessageFields.JSONWBBMessage.DISTRICT, TestParameters.DISTRICT);
    vote.put(VoteMessage.START_EVM_SIG, TestParameters.BOOTH_SIGNATURE);

    JSONArray raceArray = new JSONArray();

    JSONObject la = new JSONObject();
    la.put(VoteMessage.RACE_ID, "la");
    la.put(VoteMessage.PREFERENCES, new JSONArray("[\"1\",\" \",\" \"]"));
    raceArray.put(la);

    JSONObject lcATL = new JSONObject();
    lcATL.put(VoteMessage.RACE_ID, "lc_atl");
    lcATL.put(VoteMessage.PREFERENCES, new JSONArray("[\"1\",\" \",\" \"]"));
    raceArray.put(lcATL);

    JSONObject lcBTL = new JSONObject();
    lcBTL.put(VoteMessage.RACE_ID, "lc_btl");
    lcBTL.put(VoteMessage.PREFERENCES, new JSONArray("[\" \",\" \",\" \"]"));
    raceArray.put(lcBTL);

    vote.put(VoteMessage.RACES, raceArray);

    // Test the servlet.
    Client client = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    MBBProxyServlet fixture = new MBBProxyServlet(client);

    Map<String, String> params = new Hashtable<String, String>();
    params.put("msg", vote.toString());

    Response result = fixture.runServlet(null, null, null, params, null, null);
    assertNotNull(result);

    assertEquals(Response.Status.OK, result.getStatus());
    assertEquals(VVoteNanoHTTPD.MIME_JSON, result.getMimeType());
  }
}