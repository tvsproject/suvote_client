/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigInteger;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * The class <code>CandidateCipherTextTest</code> contains tests for the class <code>{@link CandidateCipherText}</code>.
 */
public class CandidateCipherTextTest {

  /**
   * Run the CandidateCipherText(ECGroup,JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = org.json.JSONException.class)
  public void testCandidateCipherText_1() throws Exception {
    CandidateCipherText result = new CandidateCipherText(TestParameters.getInstance().getGroup(), new JSONObject());
    assertNull(result);
  }

  /**
   * Run the CandidateCipherText(ECGroup,JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateCipherText_2() throws Exception {
    CandidateCipherText result = new CandidateCipherText(TestParameters.getInstance().getGroup(), TestParameters.getInstance()
        .getCandidateIDCipherJSON());
    assertNotNull(result);
  }

  /**
   * Run the int compareTo(CandidateCipherText) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompareTo_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());
    CandidateCipherText compare = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());
    compare.reencrypt(TestParameters.getInstance().getPublicKey(), BigInteger.TEN);

    assertEquals(0, fixture.compareTo(fixture));
    assertFalse(fixture.compareTo(compare) == 0);
  }

  /**
   * Run the JSONObject getAsJSON() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetAsJSON_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    JSONObject cipher = fixture.getAsJSON();
    assertEquals(TestParameters.getInstance().getCandidateIDCipherJSON().toString(), cipher.toString());
  }

  /**
   * Run the ECPoint[] getCipher() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCipher_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    ECPoint[] cipher = fixture.getCipher();

    for (int i = 0; i < cipher.length; i++) {
      assertEquals(TestParameters.getInstance().getCandidateIDCipher()[i], cipher[i]);
    }
  }

  /**
   * Run the BigInteger getGREncoded() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetGREncoded_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    BigInteger gr = fixture.getGREncoded();
    assertEquals(new BigInteger(TestParameters.getInstance().getCandidateIDCipher()[0].getEncoded()), gr);
  }

  /**
   * Run the BigInteger getMYREncoded() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetMYREncoded_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    BigInteger myr = fixture.getMYREncoded();
    assertEquals(new BigInteger(TestParameters.getInstance().getCandidateIDCipher()[1].getEncoded()), myr);
  }

  /**
   * Run the int getPermutationIndex() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetPermutationIndex_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    fixture.setPermutationIndex(99);
    assertEquals(99, fixture.getPermutationIndex());
  }

  /**
   * Run the void reencrypt(ECPoint,BigInteger) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testReencrypt_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    BigInteger gr = fixture.getGREncoded();
    BigInteger myr = fixture.getGREncoded();

    fixture.reencrypt(TestParameters.getInstance().getPublicKey(), BigInteger.TEN);

    assertFalse(gr == fixture.getGREncoded());
    assertFalse(myr == fixture.getMYREncoded());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    BigInteger gr = fixture.getGREncoded();
    BigInteger myr = fixture.getGREncoded();

    fixture.setReencryptData(TestParameters.getInstance().getPublicKey(), BigInteger.TEN);
    fixture.run();

    assertFalse(gr == fixture.getGREncoded());
    assertFalse(myr == fixture.getMYREncoded());
  }

  /**
   * Run the void setReencryptData(ECPoint,BigInteger) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetReencryptData_1() throws Exception {
    CandidateCipherText fixture = new CandidateCipherText(new ECUtils(), TestParameters.getInstance().getCandidateIDCipherJSON());

    fixture.setReencryptData(TestParameters.getInstance().getPublicKey(), BigInteger.TEN);
    assertEquals(BigInteger.TEN, fixture.getRandValue());
  }
}