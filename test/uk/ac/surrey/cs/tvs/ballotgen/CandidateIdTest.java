/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>CandidateIdTest</code> contains tests for the class <code>{@link CandidateId}</code>.
 */
public class CandidateIdTest {

  /**
   * Run the CandidateId(JSONObject) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCandidateId_1() throws Exception {
    JSONObject candId = new JSONObject();

    CandidateId result = new CandidateId(candId);
    assertNotNull(result);
  }

  /**
   * Run the CandidateCipherText getNewCipherText() method test.
   * 
   * @throws Exception
   */
  @Test(expected = org.json.JSONException.class)
  public void testGetNewCipherText_1() throws Exception {
    CandidateId fixture = new CandidateId(new JSONObject());

    CandidateCipherText result = fixture.getNewCipherText();
    assertNull(result);
  }

  /**
   * Run the CandidateCipherText getNewCipherText() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetNewCipherText_2() throws Exception {
    CandidateId fixture = new CandidateId(TestParameters.getInstance().getCandidateIDCipherJSON());

    CandidateCipherText result = fixture.getNewCipherText();
    assertNotNull(result);
  }
}