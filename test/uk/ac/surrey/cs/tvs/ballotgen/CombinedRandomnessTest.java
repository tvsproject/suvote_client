/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>CombinedRandomnessTest</code> contains tests for the class <code>{@link CombinedRandomness}</code>.
 */
public class CombinedRandomnessTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Run the void closeAllFiles() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCloseAllFiles_1() throws Exception {
    JSONObject keyPair = IOUtils.readJSONObjectFromFile(TestParameters.DEVICE_KEY_PAIR_FILE);
    RandomnessFiles randomnessFiles = new RandomnessFiles("rubbish");
    randomnessFiles.findRandomnessFiles();

    CombinedRandomness fixture = new CombinedRandomness(keyPair, randomnessFiles, TestParameters.AES_ALGORITHM);

    fixture.closeAllFiles();
  }

  /**
   * Run the CombinedRandomness(JSONObject,RandomnessFiles) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCombinedRandomness_1() throws Exception {
    JSONObject keyPair = IOUtils.readJSONObjectFromFile(TestParameters.DEVICE_KEY_PAIR_FILE);
    RandomnessFiles randomnessFiles = new RandomnessFiles(TestParameters.RANDOM_FOLDER);

    CombinedRandomness result = new CombinedRandomness(keyPair, randomnessFiles, TestParameters.AES_ALGORITHM);
    assertNotNull(result);
    assertNull(result.getCurrentSerialNo());

    result.closeAllFiles();
  }

  /**
   * Run the JSONArray findSerial(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testFindSerial_1() throws Exception {
    JSONObject keyPair = IOUtils.readJSONObjectFromFile(TestParameters.DEVICE_KEY_PAIR_FILE);
    RandomnessFiles randomnessFiles = new RandomnessFiles(TestParameters.RANDOM_FOLDER);
    randomnessFiles.findRandomnessFiles();

    CombinedRandomness fixture = new CombinedRandomness(keyPair, randomnessFiles, TestParameters.AES_ALGORITHM);

    // Find serial numbers in different orders
    for (int i = 0; i < TestParameters.SERIAL_NUMBERS.length; i++) {
      JSONArray result = fixture.findSerial(TestParameters.SERIAL_NUMBERS[i]);
      assertEquals(TestParameters.RANDOMNESS_QUANTITY, result.length());

      assertTrue(result.getJSONObject(0).has("serialNo"));
      assertEquals(TestParameters.SERIAL_NUMBERS[i], result.getJSONObject(0).get("serialNo"));

      assertTrue(result.getJSONObject(0).has("randomness"));
    }

    fixture.closeAllFiles();
  }

  /**
   * Run the JSONArray findSerial(String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.SerialNumberNotFoundException.class)
  public void testFindSerial_2() throws Exception {
    JSONObject keyPair = IOUtils.readJSONObjectFromFile(TestParameters.DEVICE_KEY_PAIR_FILE);
    RandomnessFiles randomnessFiles = new RandomnessFiles(TestParameters.RANDOM_FOLDER);
    randomnessFiles.findRandomnessFiles();

    CombinedRandomness fixture = new CombinedRandomness(keyPair, randomnessFiles, TestParameters.AES_ALGORITHM);

    // Test failure to find a serial number.
    JSONArray result = fixture.findSerial("rubbish");
    assertEquals(0, result.length());

    fixture.closeAllFiles();
  }

  /**
   * Run the void nextRow() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testNextRow_1() throws Exception {
    JSONObject keyPair = IOUtils.readJSONObjectFromFile(TestParameters.DEVICE_KEY_PAIR_FILE);
    RandomnessFiles randomnessFiles = new RandomnessFiles(TestParameters.RANDOM_FOLDER);
    randomnessFiles.findRandomnessFiles();

    CombinedRandomness fixture = new CombinedRandomness(keyPair, randomnessFiles, TestParameters.AES_ALGORITHM);

    assertNull(fixture.getCurrentSerialNo());

    // Test iterating through all rows.
    for (int i = 0; i < TestParameters.RANDOM_VALUES; i++) {
      fixture.nextRow();
      assertNotNull(fixture.getCurrentSerialNo());
      assertEquals(TestParameters.DEVICE_NAME + ":" + (i + 1), fixture.getCurrentSerialNo());

      assertNotNull(fixture.getNextCombinedValue());
    }
  }
}