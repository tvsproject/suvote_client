/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.PretendMBB;
import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.BallotAuditCommitMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>BallotGenAuditTest</code> contains tests for the class <code>{@link BallotGenAudit}</code>.
 */
public class BallotGenAuditTest {

  /** Progress value. */
  private int progress = 0;

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Initialise the security provider.
    TestParameters.getInstance();

    // Generate the test audit file.
    JSONArray serials = new JSONArray();

    serials.put("TestDeviceOne:1");
    serials.put("TestDeviceOne:3");
    serials.put("TestDeviceOne:83");
    serials.put("TestDeviceOne:125");
    serials.put("TestDeviceOne:222");
    serials.put("TestDeviceOne:398");
    serials.put("TestDeviceOne:412");
    serials.put("TestDeviceOne:599");
    serials.put("TestDeviceOne:621");
    serials.put("TestDeviceOne:748");
    serials.put("TestDeviceOne:808");
    serials.put("TestDeviceOne:964");
    serials.put("TestDeviceOne:1000");

    JSONObject audit = new JSONObject();
    audit.put("auditSerials", serials);
    JSONArray ballots = new JSONArray();
    IOUtils.writeJSONToFile(audit, TestParameters.AUDIT_FILE);
    JSONObject obj = new JSONObject();
    for (int i = 0; i < serials.length(); i++) {
      IOUtils.writeJSONToFile(obj,
          TestParameters.OUTPUT_BALLOT_FOLDER + "/" + IOUtils.getFileNameFromSerialNo(serials.getString(i)) + ".json");
      ballots.put(TestParameters.OUTPUT_BALLOT_FOLDER + "/" + IOUtils.getFileNameFromSerialNo(serials.getString(i)) + ".json");
    }
    IOUtils.writeJSONToFile(ballots, TestParameters.OUTPUT_BALLOT_LIST_FILE);

  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Tests that the correct audit file has been created and that its content is as expected.
   * 
   * @param auditFile
   *          The input audit file.
   * @param outputFile
   *          The audit output file.
   * @throws IOException
   * @throws JSONException
   */
  private void testAuditFile(String auditFile, String outputFile) throws IOException, JSONException {
    // Test that each file has the correct number of content.
    int outCount = 0;

    BufferedReader inReader = null;
    BufferedReader outReader = null;

    try {
      inReader = new BufferedReader(new FileReader(auditFile));
      outReader = new BufferedReader(new FileReader(outputFile));

      // The audit file contains a single line.
      String inLine = inReader.readLine();
      JSONObject auditObject = new JSONObject(inLine);
      JSONArray auditSerials = auditObject.getJSONArray(TestParameters.AUDIT_SERIALS_KEY);

      String outLine = outReader.readLine();

      while (outLine != null) {
        outCount++;

        // Convert the line into a JSON array - there should be three entries.
        JSONArray randomnessArray = new JSONArray(outLine);
        assertEquals(TestParameters.RANDOMNESS_QUANTITY, randomnessArray.length());

        JSONObject randomObject = randomnessArray.getJSONObject(0);

        // Check the content.
        assertTrue(randomObject.has(MessageFields.RandomnessFile.SERIAL_NO));
        assertTrue(randomObject.has(MessageFields.RandomnessFile.RANDOMNESS));
        assertTrue(randomObject.has(MessageFields.RandomnessFile.PEER_ID));

        JSONArray randomArray = (JSONArray) randomObject.get(MessageFields.RandomnessFile.RANDOMNESS);
        assertEquals(TestParameters.COLUMNS, randomArray.length());

        outLine = outReader.readLine();
      }

      // Check the number of objects: only 1 each.
      assertEquals(auditSerials.length(), outCount);
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
      if (outReader != null) {
        outReader.close();
      }
    }
  }

  /**
   * Run the BallotGenAudit(BallotGenConf,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenAuditException.class)
  public void testBallotGenAudit_1() throws Exception {
    BallotGenConf ballotGenConfFile = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);

    BallotGenAudit result = new BallotGenAudit(ballotGenConfFile, "rubbish", "rubbish", new ConfigFile(
        TestParameters.OUTPUT_CLIENT_CONFIG_FILE));
    assertNull(result);
  }

  /**
   * Run the BallotGenAudit(BallotGenConf,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenAudit_2() throws Exception {
    BallotGenConf ballotGenConfFile = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);

    BallotGenAudit result = new BallotGenAudit(ballotGenConfFile, TestParameters.DEVICE_KEY_PAIR_FILE,
        TestParameters.RANDOM_FOLDER, new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));
    assertNotNull(result);
  }

  /**
   * Run the void doAudit(String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenAuditException.class)
  public void testDoAudit_1() throws Exception {
    BallotGenConf ballotGenConfFile = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);
    BallotGenAudit fixture = new BallotGenAudit(ballotGenConfFile, TestParameters.DEVICE_KEY_PAIR_FILE,
        TestParameters.RANDOM_FOLDER, new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    fixture.doAudit("rubbish", TestParameters.OUTPUT_AUDIT_FILE);
  }

  /**
   * Run the void doAudit(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoAudit_2() throws Exception {
    BallotGenConf ballotGenConfFile = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);
    BallotGenAudit fixture = new BallotGenAudit(ballotGenConfFile, TestParameters.DEVICE_KEY_PAIR_FILE,
        TestParameters.RANDOM_FOLDER, new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Initialise the security provider.
    TestParameters.getInstance();

    ProgressListener listener = new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        assertTrue(value >= BallotGenAuditTest.this.progress);
        BallotGenAuditTest.this.progress = value;
      }
    };

    fixture.addProgressListener(listener);
    this.progress = 0;

    fixture.doAudit(TestParameters.AUDIT_FILE, TestParameters.OUTPUT_AUDIT_FILE);

    fixture.removeProgressListener(listener);

    // Test that the audit file has be written with the correct content.
    this.testAuditFile(TestParameters.AUDIT_FILE, TestParameters.OUTPUT_AUDIT_FILE);
  }

  /**
   * Run the JSONObject submitAuditToWBB(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSubmitAuditToWBB_1() throws Exception {
    BallotGenConf ballotGenConfFile = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);
    BallotGenAudit fixture = new BallotGenAudit(ballotGenConfFile, TestParameters.DEVICE_KEY_PAIR_FILE,
        TestParameters.RANDOM_FOLDER, new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB();

    // Audit and submit the response.
    fixture.doAudit(TestParameters.AUDIT_FILE, TestParameters.OUTPUT_AUDIT_FILE);
    JSONObject result = fixture.submitAuditToWBB(TestParameters.OUTPUT_AUDIT_FILE, TestParameters.OUTPUT_AUDIT_MBB_ZIP_FILE);
    assertNotNull(result);

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    // Check what the MBB received.
    // Shutdown and check the MBB received messages and data.
    for (PretendMBB pretendMBB : pretendMBBs) {
      // Test that the correct messages were sent. Only some content is tested.
      List<JSONObject> messages = pretendMBB.getMessages();
      assertEquals(1, messages.size());

      for (JSONObject message : messages) {
        assertTrue(message.has(JSONWBBMessage.TYPE));
        assertTrue(message.has(FileMessage.SENDER_ID));
        assertTrue(message.has(ClientConstants.BallotGenCommitResponse.SUBMISSION_ID));
        assertTrue(message.has(FileMessage.SENDER_SIG));
        assertTrue(message.has(FileMessage.DIGEST));
        assertTrue(message.has(FileMessage.FILESIZE));

        assertEquals(MessageTypes.BALLOT_AUDIT_COMMIT, message.get(JSONWBBMessage.TYPE));
        assertEquals(TestParameters.DEVICE_NAME, message.get(FileMessage.SENDER_ID));
      }

      // And that the data was also sent. Content is not tested.
      List<byte[]> data = pretendMBB.getData();
      assertEquals(1, data.size());
    }

    // Test that the content of the output ZIP file is the same as the audit output file.
    File zipFolder = new File(TestParameters.OUTPUT_ZIP_FOLDER);
    TestParameters.extractZip(new File(TestParameters.OUTPUT_AUDIT_MBB_ZIP_FILE), zipFolder);
    File[] zipFiles = zipFolder.listFiles();
    Arrays.sort(zipFiles);

    File zipAuditFile = new File(zipFolder, BallotAuditCommitMessage.AUDIT_DATA_ZIP_ENTRY);
    // TODO CJC: Changed this because we also include the WBBResponse file
    assertEquals(2, zipFiles.length);
    // TODO This needs changing to reflect that the data in the zip file is now a constant
    // BallotAuditCommitMessage.AUDIT_DATA_ZIP_ENTRY this was required to always be able to distinguish the two files on the peers
    assertEquals(zipAuditFile, zipFiles[0]);
    
    assertTrue(TestParameters.compareFile(new File(TestParameters.OUTPUT_AUDIT_FILE), zipAuditFile));
  }

  /**
   * Run the JSONObject submitAuditToWBB(String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenAuditException.class)
  public void testSubmitAuditToWBB_2() throws Exception {
    BallotGenConf ballotGenConfFile = new BallotGenConf(TestParameters.BALLOT_GENERATION_CONFIG_FILE);
    BallotGenAudit fixture = new BallotGenAudit(ballotGenConfFile, TestParameters.DEVICE_KEY_PAIR_FILE,
        TestParameters.RANDOM_FOLDER, new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Audit and submit the response.
    fixture.doAudit(TestParameters.AUDIT_FILE, TestParameters.OUTPUT_AUDIT_FILE);
    JSONObject result = fixture.submitAuditToWBB(TestParameters.OUTPUT_AUDIT_FILE, TestParameters.OUTPUT_AUDIT_MBB_ZIP_FILE);
    assertNull(result);
  }
}