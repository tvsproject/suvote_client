/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>FindSerialNoExceptionTest</code> contains tests for the class <code>{@link FindSerialNoException}</code>.
 */
public class FindSerialNoExceptionTest {

  /**
   * Run the FindSerialNoException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFindSerialNoException_1() throws Exception {

    FindSerialNoException result = new FindSerialNoException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.FindSerialNoException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the FindSerialNoException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFindSerialNoException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    FindSerialNoException result = new FindSerialNoException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.FindSerialNoException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the FindSerialNoException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFindSerialNoException_3() throws Exception {
    Throwable cause = new Throwable();

    FindSerialNoException result = new FindSerialNoException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.FindSerialNoException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the FindSerialNoException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFindSerialNoException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    FindSerialNoException result = new FindSerialNoException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.FindSerialNoException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the FindSerialNoException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testFindSerialNoException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    FindSerialNoException result = new FindSerialNoException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.FindSerialNoException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}