/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>BallotGenerationExceptionTest</code> contains tests for the class <code>{@link BallotGenerationException}</code>.
 */
public class BallotGenerationExceptionTest {

  /**
   * Run the BallotGenerationException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenerationException_1() throws Exception {

    BallotGenerationException result = new BallotGenerationException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenerationException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the BallotGenerationException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenerationException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    BallotGenerationException result = new BallotGenerationException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenerationException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the BallotGenerationException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenerationException_3() throws Exception {
    Throwable cause = new Throwable();

    BallotGenerationException result = new BallotGenerationException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenerationException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the BallotGenerationException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenerationException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    BallotGenerationException result = new BallotGenerationException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenerationException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the BallotGenerationException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBallotGenerationException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    BallotGenerationException result = new BallotGenerationException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.BallotGenerationException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}