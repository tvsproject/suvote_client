/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>ProxyMessageProcessingExceptionTest</code> contains tests for the class
 * <code>{@link ProxyMessageProcessingException}</code>.
 * 
 * @generatedBy CodePro at 21/08/13 09:57
 */
public class ProxyMessageProcessingExceptionTest {

  /**
   * Run the ProxyMessageProcessingException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyMessageProcessingException_1() throws Exception {

    ProxyMessageProcessingException result = new ProxyMessageProcessingException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the ProxyMessageProcessingException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyMessageProcessingException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    ProxyMessageProcessingException result = new ProxyMessageProcessingException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the ProxyMessageProcessingException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyMessageProcessingException_3() throws Exception {
    Throwable cause = new Throwable();

    ProxyMessageProcessingException result = new ProxyMessageProcessingException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the ProxyMessageProcessingException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyMessageProcessingException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    ProxyMessageProcessingException result = new ProxyMessageProcessingException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the ProxyMessageProcessingException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProxyMessageProcessingException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    ProxyMessageProcessingException result = new ProxyMessageProcessingException(message, cause, enableSuppression,
        writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.ballotgen.exceptions.ProxyMessageProcessingException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}