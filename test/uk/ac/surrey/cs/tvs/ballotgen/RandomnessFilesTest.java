/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ballotgen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Collections;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;

/**
 * The class <code>RandomnessFilesTest</code> contains tests for the class <code>{@link RandomnessFiles}</code>.
 */
public class RandomnessFilesTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Initialise the security provider.
    TestParameters.getInstance();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void findRandomnessFiles() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testFindRandomnessFiles_1() throws Exception {
    RandomnessFiles fixture = new RandomnessFiles(TestParameters.RANDOM_FOLDER);

    fixture.findRandomnessFiles();

    List<String> output = fixture.getAESKeys();
    assertNotNull(output);
    assertEquals(TestParameters.RANDOMNESS_QUANTITY, output.size());
    assertTrue(output.get(0).endsWith(TestParameters.DEVICE_AES_FILE));

    output = fixture.getCommitFiles();
    assertNotNull(output);
    assertEquals(TestParameters.RANDOMNESS_QUANTITY, output.size());
    assertTrue(output.get(0).endsWith(TestParameters.DEVICE_COMMIT_DATA_FILE));

    output = fixture.getPeerIDS();
    assertNotNull(output);
    Collections.sort(output);
    assertEquals(TestParameters.RANDOMNESS_QUANTITY, output.size());
    assertEquals(TestParameters.RANDOMNESS_SOURCE, output.get(0));

    output = fixture.getRandomnessFiles();
    assertNotNull(output);
    assertEquals(TestParameters.RANDOMNESS_QUANTITY, output.size());
    assertTrue(output.get(0).endsWith(TestParameters.DEVICE_RANDOM_DATA_FILE));
  }

  /**
   * Run the RandomnessFiles(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessFiles_1() throws Exception {
    RandomnessFiles result = new RandomnessFiles(TestParameters.RANDOM_FOLDER);
    assertNotNull(result);

    List<String> output = result.getAESKeys();
    assertNotNull(output);
    assertEquals(0, output.size());

    output = result.getCommitFiles();
    assertNotNull(output);
    assertEquals(0, output.size());

    output = result.getPeerIDS();
    assertNotNull(output);
    assertEquals(0, output.size());

    output = result.getRandomnessFiles();
    assertNotNull(output);
    assertEquals(0, output.size());
  }

  /**
   * Run the boolean verifyAllFilesAndSignatures(ConfigFile) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testVerifyAllFilesAndSignatures_1() throws Exception {
    RandomnessFiles fixture = new RandomnessFiles(TestParameters.RANDOM_FOLDER);

    fixture.findRandomnessFiles();

    assertTrue(fixture.verifyAllFilesAndSignatures(new ConfigFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE)));
  }
}