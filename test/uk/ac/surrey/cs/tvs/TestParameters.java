/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.crypto.Cipher;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * This class defines common test parameters and static initialisers within a singleton.
 */
public class TestParameters {

  /** The number of candidate ids to be created. */
  public static final int       CANDIDATES                       = 80;

  /** The name of the randomness source peer. */
  public static final String    RANDOMNESS_SOURCE                = "MixServer1";

  /** The peer id matching the server_conf.json. */
  public static final String[]    PEER_ID                          = {"Peer1","Peer2","Peer3"};

  /** Input folder for random files. */
  public static final String    RANDOM_FOLDER                    = "./testdata/randomness";

  /** AES key file. */
  public static String          DEVICE_AES_FILE                  = "AESKey.json";

  /** AES algorithm. */
  public static String          AES_ALGORITHM                    = "AES/ECB/NoPadding";

  /** Output file for the randomness data file. */
  public static String          DEVICE_RANDOM_DATA_FILE          = "randomData.json";

  /** Output file for the commit data file. */
  public static String          DEVICE_COMMIT_DATA_FILE          = "commitData.json";

  /** Input file for the public key. */
  public static final String    PUBLIC_KEY_FILE                  = "./testdata/publickey.json";

  /** Input file for auditing. */
  public static final String    AUDIT_FILE                       = "./testdata/ballotsToAudit.json";

  /** Input key for audit serial numbers. */
  public static final String    AUDIT_SERIALS_KEY                = "auditSerials";

  /** Number of columns in randomness. */
  public static final int       COLUMNS                          = 80;

  /** Configuration file for ballot generation. */
  public static final String    BALLOT_GENERATION_CONFIG_FILE    = "./testdata/ballot_gen_conf.json";

  /** Configuration file for the client. */
  public static final String    CLEAN_CLIENT_CONFIG_FILE         = "./testdata/clean_client.conf";
  public static final String    CLEAN_CLIENT_CONFIG_FILE_NO_KEY         = "./testdata/keygen_client.conf";

  /** District configuration file. */
  public static final String    DISTRICT_CONFIG_FILE             = "./testdata/districtconf.json";

  /** Test booth. */
  public static final String    BOOTH                            = "booth";

  /** Test booth. */
  public static final String    BOOTH_SIGNATURE                  = "boothSig";

  /** Test district. */
  public static final String    DISTRICT                         = "Gembrook";

  /** Proxy server ports. We use multiple ports because they take some time to be released. */
  public static final int[]     PROXY_PORTS                      = new int[] { 8060, 8061, 8062, 8063 };

  /** Client ports. We use multiple ports because they take some time to be released. */
  public static final int[]     CLIENT_PORTS                     = new int[] { 7060, 7061, 7062, 7063 };

  /** Proxy server root. */
  public static final String    PROXY_ROOT                       = "./wwwroot/";

  /** Test candidates per race. */
  public static final int[]     RACE_CANDIDATES                  = new int[] { 15, 10, 54 };

  /** Number of ballots. */
  public static final int       BALLOTS                          = 100;

  /** Test device name. */
  public static final String    DEVICE_NAME                      = "TestDeviceOne";
  public static final String    RANDOMSERVER_NAME                = "MixServer1";

  /** Test serial numbers to search for. */
  public static final String[]  SERIAL_NUMBERS                   = new String[] { DEVICE_NAME + ":" + 12, DEVICE_NAME + ":" + 568,
      DEVICE_NAME + ":" + 129, DEVICE_NAME + ":" + 1, DEVICE_NAME + ":" + 1000 };

  /** Number of random values. */
  public static final int       RANDOM_VALUES                    = 1000;

  /** File holding the test device key pair. */
  public static final String    DEVICE_KEY_PAIR_FILE             = "./testdata/keyPair.json";

  /** Development certificate file. */
  public static final String    CERTIFICATE_FILE                 = "./testdata/certs/TestDeviceOne.pem";

  /** Development SSL certificate file. */
  public static final String    SSL_CERTIFICATE_FILE             = "./testdata/certs/TestDeviceOneSSL.pem";

  /** Configuration file for the client. */
  public static final String    OUTPUT_CLIENT_CONFIG_FILE        = "./testdata/new_client.conf";
  public static final String    OUTPUT_CLIENT_CONFIG_FILE_NO_KEY        = "./testdata/new_client_no_key.conf";

  /** Output file for the test client signing key. */
  public static final String    OUTPUT_CLIENT_SIGNING_KEY_FILE   =  DEVICE_NAME + "SigningKey.json";

  /** Output file for the test client public key. */
  public static final String    OUTPUT_CLIENT_PUBLIC_KEY_FILE    =  DEVICE_NAME + "_CryptoPublicKey.json";

  /** Output file for the test client key pair. */
  public static final String    OUTPUT_CLIENT_KEY_PAIR_FILE      =  DEVICE_NAME + "_CryptoKeyPair.json";

  /** Output file for the test client certificate signing request. */
  public static final String    OUTPUT_CLIENT_CERT_SIGNING_FILE  =  DEVICE_NAME + "CertReq.csr";

  /** Saved keys folder. */
  public static final String    SAVED_KEYS                       = "./testdata/";

  /** Output folder. */
  public static final String    OUTPUT_FOLDER                    = "./testdata/outputs";

  /** Client status parameters. */
  public static final String[]  CLIENT_STATUS_PARAMS             = new String[] { "SigningKeyStore", "SSLCSRLocation",
      "CertificateLocation", "EncryptionKeyStore", "BallotsGenerated", "BallotsAudited", "Initialised" };

  /** Output file for plain text ids. */
  public static final String    OUTPUT_PLAIN_TEXT_FILE           = "./testdata/plaintexts_ids.json";

  /** Output file for encrypted ids. */
  public static final String    OUTPUT_ENCRYPTED_FILE            = "./testdata/base_encrypted_ids.json";

  /** Output file for audit. */
  public static final String    OUTPUT_AUDIT_FILE                = "ballotsGenAudit.json";

  /** Output file for audit response. */
  public static final String    OUTPUT_AUDIT_RESPONSE_FILE       = "./testdata/auditOutput.json";

  /** Output file for audit submission to MBB. */
  public static final String    OUTPUT_AUDIT_MBB_ZIP_FILE        = "./testdata/wbbauditsubmission.zip";

  /** Output file for ballot submission to MBB. */
  public static final String    OUTPUT_BALLOT_MBB_ZIP_FILE       = "./testdata/wbbsubmission.zip";

  /** Output Zip folder when testing Zip content. */
  public static final String    OUTPUT_ZIP_FOLDER                = "zip";

  /** Output ballot folder. */
  public static final String    OUTPUT_BALLOT_FOLDER             = "./testdata/ballots";

  /** Output file for ballot ciphers. */
  public static final String    OUTPUT_BALLOT_CIPHERS_FILE       = "wbbciphers.json";

  /** Output file for ballot ciphers printed. */
  public static final String    OUTPUT_BALLOT_CIPHERS_PRINT_FILE = "./testdata/print_ciphers.json";

  /** Output file for the list of ballots. */
  public static final String    OUTPUT_BALLOT_LIST_FILE          = "./testdata/ballots.json";

  /** Output file for the ballot database. */
  public static final String    OUTPUT_BALLOT_DB_FILE            = "./testdata/ballotDB.json";

  /** Dummy exception message used for testing. */
  public static final String    EXCEPTION_MESSAGE                = "Exception Message";

  /** Test commit time. */
  public static final String    COMMIT_TIME                      = "18:00";

  /** The number of sets of randomness files. */
  public static final int       RANDOMNESS_QUANTITY              = 5;

  /** UI folder. */
  public static final String    UI_FOLDER                        = "./wwwroot/ui/";

  /** Local port file in UI folder. */
  public static final String    LOCALPORT_FILE                   = "js/localport.json";

  /** New certificate folder. */
  public static final String    NEW_CERTS_FOLDER                 = "new_certs";

  /** Default buffer size to use when copying files. */
  private static final int      BUFFER_SIZE                      = 8192;

  /** The singleton instance of these parameters. */
  private static TestParameters instance                         = null;

  /** Test EC group. */
  private ECUtils               group                            = null;

  /** Test candidate ID. */
  private ECPoint               candidateID                      = null;

  /** Test candidate ID cipher. */
  private ECPoint[]             candidateIDCipher                = null;

  /** Test public key. */
  private ECPoint               publicKey                        = null;

  /** Test randomness. */
  private JSONObject            randomness                       = null;

  /** Test commitment. */
  private JSONObject            commitment                       = null;

  /** Test decrypt cipher. */
  private Cipher                decryptCipher                    = null;

  /**
   * Private default constructor to prevent instantiation.
   */
  private TestParameters() {
    super();

    // Initialise BouncyCastle.
    CryptoUtils.initProvider();
  }

  /**
   * @return Test candidate ID.
   */
  public ECPoint getCandidateID() {
    // Lazy creation.
    if (this.candidateID == null) {
      this.candidateID = this.getGroup().getRandomValue();
    }

    return this.candidateID;
  }

  /**
   * @return The test candidate ID as a cipher.
   * @throws JSONIOException
   * @throws JSONException
   */
  public ECPoint[] getCandidateIDCipher() throws JSONIOException, JSONException {
    // Lazy creation.
    if (this.candidateIDCipher == null) {
      this.candidateIDCipher = this.getGroup().encrypt(this.getCandidateID(), this.getPublicKey(), BigInteger.ONE);
    }

    return this.candidateIDCipher;
  }

  /**
   * @return The test candidate ID cipher as a JSON object.
   * @throws JSONIOException
   * @throws JSONException
   */
  public JSONObject getCandidateIDCipherJSON() throws JSONException, JSONIOException {
    return this.getGroup().cipherToJSON(this.getCandidateIDCipher());
  }

  /**
   * @return Test candidate ID as a JSON object.
   * @throws JSONException
   */
  public JSONObject getCandidateIDJSON() throws JSONException {
    return ECUtils.ecPointToJSON(this.getCandidateID());
  }

  /**
   * @return A valid commitment for a device which is associated with a randomness value and its decrypt key.
   * 
   * @throws IOException
   * @throws JSONException
   */
  public JSONObject getCommitment() throws IOException, JSONException {
    // Lazy creation.
    if (this.commitment == null) {
      BufferedReader inReader = null;

      try {
        inReader = new BufferedReader(new FileReader(RANDOM_FOLDER + "/" + RANDOMSERVER_NAME + "/" + DEVICE_COMMIT_DATA_FILE));

        // Read in the first object.
        String inLine = inReader.readLine();
        this.commitment = new JSONObject(inLine);
      }
      // Pass all exceptions up.
      finally {
        if (inReader != null) {
          inReader.close();
        }
      }
    }

    return this.commitment;
  }

  /**
   * @return A valid decryption cipher for a device which is associated with a randomness value and its commitment.
   * @throws JSONIOException
   * @throws CryptoIOException
   */
  public Cipher getDecryptCipher() throws JSONIOException, CryptoIOException {
    // Lazy creation.
    if (this.decryptCipher == null) {
      JSONObject keyPair = IOUtils.readJSONObjectFromFile(DEVICE_KEY_PAIR_FILE);
      JSONObject aesKey = IOUtils.readJSONObjectFromFile(RANDOM_FOLDER + "/" + RANDOMSERVER_NAME + "/" + DEVICE_AES_FILE);

      this.decryptCipher = CryptoUtils.getAESDecryptionCipherFromEncKey(aesKey, keyPair);
    }

    return this.decryptCipher;
  }

  /**
   * @return The test EC group.
   */
  public ECUtils getGroup() {
    // Lazy creation.
    if (this.group == null) {
      this.group = new ECUtils();
    }

    return this.group;
  }

  /**
   * @return Test public key.
   * @throws JSONIOException
   * @throws JSONException
   */
  public ECPoint getPublicKey() throws JSONIOException, JSONException {
    // Lazy creation.
    if (this.publicKey == null) {
      JSONObject pkJson = IOUtils.readJSONObjectFromFile(TestParameters.PUBLIC_KEY_FILE);
      this.publicKey = this.getGroup().pointFromJSON(pkJson);
    }

    return this.publicKey;
  }

  /**
   * @return A valid randomness value for a device which is associated with a commitment and its decrypt key.
   * 
   * @throws IOException
   * @throws JSONException
   */
  public JSONObject getRandomness() throws IOException, JSONException {
    // Lazy creation.
    if (this.randomness == null) {
      BufferedReader inReader = null;

      try {
        inReader = new BufferedReader(new FileReader(RANDOM_FOLDER + "/" + RANDOMSERVER_NAME + "/" + DEVICE_RANDOM_DATA_FILE));

        // Read in the first object.
        String inLine = inReader.readLine();
        this.randomness = new JSONObject(inLine);
      }
      // Pass all exceptions up.
      finally {
        if (inReader != null) {
          inReader.close();
        }
      }
    }

    return this.randomness;
  }

  /**
   * Compares the content of two files.
   * 
   * @param file1
   *          Input file 1.
   * @param file2
   *          Input file 2.
   * @return True if the files are identical, false otherwise.
   * @throws IOException
   *           on failure to read.
   */
  public static boolean compareFile(File file1, File file2) throws IOException {
    boolean result = true;

    byte[] buffer1 = new byte[BUFFER_SIZE];
    byte[] buffer2 = new byte[BUFFER_SIZE];

    InputStream in1 = null;
    InputStream in2 = null;

    try {
      in1 = new FileInputStream(file1);
      in2 = new FileInputStream(file2);

      int numBytes1 = in1.read(buffer1);
      int numBytes2 = in2.read(buffer2);

      while ((numBytes1 != -1) && (numBytes2 != -1) && result) {
        if (numBytes1 != numBytes2) {
          result = false;
        }
        else {
          for (int i = 0; i < numBytes1; i++) {
            if (buffer1[i] != buffer2[i]) {
              result = false;
            }
          }
        }

        numBytes1 = in1.read(buffer1);
        numBytes2 = in2.read(buffer2);
      }
    }
    // Throw exceptions up.
    finally {
      if (in1 != null) {
        in1.close();
      }
      if (in2 != null) {
        in2.close();
      }
    }

    return result;
  }

  /**
   * Copies an input file to an output file.
   * 
   * @param in
   *          The source file.
   * @param out
   *          The destination file.
   * @throws IOException
   */
  public static void copyFile(File in, File out) throws IOException {
    InputStream inStream = null;
    OutputStream outStream = null;

    try {
      inStream = new FileInputStream(in);
      outStream = new FileOutputStream(out);

      copyStream(inStream, outStream);
    }
    // Pass all exceptions up.
    finally {
      if (inStream != null) {
        inStream.close();
      }
      if (outStream != null) {
        outStream.close();
      }
    }
  }

  /**
   * Copies an input stream to an output stream. Both streams are left at the end of their read/write.
   * 
   * @param in
   *          The source stream.
   * @param out
   *          The destination stream.
   * 
   * @throws IOException
   *           on failure to read or write.
   */
  public static void copyStream(InputStream in, OutputStream out) throws IOException {
    byte[] buffer = new byte[BUFFER_SIZE];

    int numBytes = in.read(buffer);

    while (numBytes != -1) {
      out.write(buffer, 0, numBytes);
      numBytes = in.read(buffer);
    }
  }

  /**
   * Recursive method to delete a folder and its content. If something goes wrong, this method remains silent.
   * 
   * @param file
   *          The file/folder to delete.
   */
  public static void deleteRecursive(File file) {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursive(child);
      }
    }

    file.delete();
  }

  /**
   * Extracts a Zip file into the specified directory.
   * 
   * @param source
   *          The ZIP file.
   * @param target
   *          The target directory.
   * @return True if the extraction was successful, false otherwise.
   * @throws IOException
   */
  public static boolean extractZip(File source, File target) throws IOException {
    // Open the ZIP archive.
    ZipInputStream zipInputStream = null;
    OutputStream outputStream = null;
    boolean success = false;

    try {
      zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(source)));

      ZipEntry zipEntry = zipInputStream.getNextEntry();

      while (zipEntry != null) {
        // Extract the file name, including any sub-directory names. Sub-directories are created.
        File outputFile = new File(target, zipEntry.getName());
        outputFile.getParentFile().mkdirs();

        // Extract the file.
        outputStream = new FileOutputStream(outputFile);
        copyStream(zipInputStream, outputStream);
        outputStream.close();
        outputStream = null;

        // Get the next entry.
        zipEntry = zipInputStream.getNextEntry();
      }

      success = true;
    }
    // Throw exceptions up.
    finally {
      // Close any open output stream.
      if (outputStream != null) {
        outputStream.close();
      }

      // Close the input stream.
      if (zipInputStream != null) {
        zipInputStream.close();
      }
    }

    return success;
  }

  /**
   * Singleton access method.
   * 
   * @return The test parameters singleton instance.
   */
  public static TestParameters getInstance() {
    // Lazy creation.
    if (instance == null) {
      instance = new TestParameters();
    }

    return instance;
  }

  /**
   * Signs the specified content using the POD to Peer1 signing key.
   * 
   * @param data
   *          The data to sign.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String data,int peer) throws TVSSignatureException, CryptoIOException {
    // PrivateKey key = CryptoUtils.getSignatureKeyFromKeyStore("./testdata/Peer1_private.bks", "", "Peer1_SigningSK2", "");
    try {
      TVSKeyStore tvsKeys = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      TVSSignature signature=null; 
      if(peer==0){
        tvsKeys.load("./testdata/Peer1_private.bks", "".toCharArray());
        signature = new TVSSignature(SignatureType.BLS, tvsKeys.getBLSPrivateKey("Peer1_SigningSK2"));
      }else if(peer==1){
        tvsKeys.load("./testdata/Peer2_private.bks", "".toCharArray());
        signature = new TVSSignature(SignatureType.BLS, tvsKeys.getBLSPrivateKey("Peer2_SigningSK2"));
      }else if(peer==2){
        tvsKeys.load("./testdata/Peer3_private.bks", "".toCharArray());
        signature = new TVSSignature(SignatureType.BLS, tvsKeys.getBLSPrivateKey("Peer3_SigningSK2"));
      }
    
      
      signature.update(data);

      return signature.signAndEncode(EncodingType.BASE64);

    }
    catch (KeyStoreException e) {
      throw new CryptoIOException(e);
    }
  }

  /**
   * Deletes all of the test output files that are generated.
   */
  public static void tidyFiles() {
    deleteRecursive(new File(OUTPUT_CLIENT_CONFIG_FILE));
    deleteRecursive(new File(OUTPUT_CLIENT_CONFIG_FILE_NO_KEY));

    deleteRecursive(new File(AUDIT_FILE));

    deleteRecursive(new File(OUTPUT_PLAIN_TEXT_FILE));
    deleteRecursive(new File(OUTPUT_ENCRYPTED_FILE));
    deleteRecursive(new File(OUTPUT_AUDIT_FILE));

    deleteRecursive(new File(OUTPUT_AUDIT_RESPONSE_FILE));

    deleteRecursive(new File(OUTPUT_AUDIT_MBB_ZIP_FILE));
    deleteRecursive(new File(OUTPUT_BALLOT_MBB_ZIP_FILE));

    deleteRecursive(new File(OUTPUT_ZIP_FOLDER));

    deleteRecursive(new File(OUTPUT_BALLOT_FOLDER));
    deleteRecursive(new File(OUTPUT_BALLOT_CIPHERS_FILE));
    deleteRecursive(new File(OUTPUT_BALLOT_LIST_FILE));
    deleteRecursive(new File(OUTPUT_BALLOT_DB_FILE));
    deleteRecursive(new File(OUTPUT_BALLOT_CIPHERS_PRINT_FILE));

    deleteRecursive(new File(NEW_CERTS_FOLDER));
  }
}
