/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>DeleteBallotWorkerTest</code> contains tests for the class <code>{@link DeleteBallotWorker}</code>.
 */
public class DeleteBallotWorkerTest {

  /**
   * Run the DeleteBallotWorker(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testDeleteBallotWorker_1() throws Exception {
    DeleteBallotWorker result = new DeleteBallotWorker("rubbish");
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    DeleteBallotWorker fixture = new DeleteBallotWorker(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    assertFalse(new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE).exists());
    fixture.run();
    assertFalse(new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE).exists());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    DeleteBallotWorker fixture = new DeleteBallotWorker(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    assertTrue(new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE).exists());
    fixture.run();
    assertFalse(new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE).exists());
  }
}