/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.PretendMBB;
import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.ballotgen.init.CandidateIDs;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;

/**
 * The class <code>ClientTest</code> contains tests for the class <code>{@link Client}</code>.
 */
public class ClientTest {

  /** Progress value. */
  private int progress = 0;

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE_NO_KEY), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE_NO_KEY));

    // Generate candidate ids.
    CandidateIDs.create(TestParameters.CANDIDATES, TestParameters.PUBLIC_KEY_FILE, TestParameters.OUTPUT_PLAIN_TEXT_FILE,
        TestParameters.OUTPUT_ENCRYPTED_FILE);
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();

    // Put back the correct key files.
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_SIGNING_KEY_FILE), new File(
        TestParameters.OUTPUT_CLIENT_SIGNING_KEY_FILE));
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_PUBLIC_KEY_FILE), new File(
        TestParameters.OUTPUT_CLIENT_PUBLIC_KEY_FILE));
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_KEY_PAIR_FILE), new File(
        TestParameters.OUTPUT_CLIENT_KEY_PAIR_FILE));
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_CERT_SIGNING_FILE), new File(
        TestParameters.OUTPUT_CLIENT_CERT_SIGNING_FILE));
  }

  /**
   * Run the Client(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testClient_1() throws Exception {
    Client result = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    assertNotNull(result);

    assertNotNull(result.getCertsLocation());
    assertNotNull(result.getCSRFileName());
    assertNull(result.getSSLCSRFileName());
  }

  /**
   * Run the Client(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException.class)
  public void testClient_2() throws Exception {
    Client result = new Client("rubbish");
    assertNull(result);
  }

  /**
   * Run the void generateBallots() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateBallots_1() throws Exception {
    Client fixture = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    // Startup a pretend MBB.
    List<PretendMBB> pretendMBBs = PretendMBB.startPrentendMBB();

    fixture.generateBallots();

    // Shutdown the MBB.
    PretendMBB.stopPretendMBB(pretendMBBs);

    assertTrue(new File(TestParameters.OUTPUT_BALLOT_DB_FILE).exists());
    assertTrue(new File(TestParameters.OUTPUT_AUDIT_RESPONSE_FILE).exists());

    // Content of ballot and audit files tested elsewhere.

    // Test the written status information.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject config = new JSONObject(inLine);

      assertTrue(config.has(TestParameters.CLIENT_STATUS_PARAMS[4]));
      assertTrue(config.has(TestParameters.CLIENT_STATUS_PARAMS[5]));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }

  /**
   * Run the void generateCryptoKeyPair() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateCryptoKeyPair_1() throws Exception {
    
    
    Client fixture = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE_NO_KEY);
    fixture.generateSigningKey();
    fixture.importSSLCertificate(TestParameters.CERTIFICATE_FILE);
    
    assertFalse(fixture.isInitialised());
    assertEquals(3, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[3], fixture.getNextTask());

    fixture.generateCryptoKeyPair();

    assertFalse(fixture.isInitialised());
    assertEquals(4, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[4], fixture.getNextTask());

    assertTrue(new File(TestParameters.OUTPUT_CLIENT_PUBLIC_KEY_FILE).exists());
    assertTrue(new File(TestParameters.OUTPUT_CLIENT_KEY_PAIR_FILE).exists());

    // CryptoPublicKey.json: single entry file.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_PUBLIC_KEY_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject publicKey = new JSONObject(inLine);

      assertTrue(publicKey.has("public"));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }

    // CryptoKeyPair.json: single entry file.
    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_KEY_PAIR_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject keyPair = new JSONObject(inLine);

      assertTrue(keyPair.has("public"));
      assertTrue(keyPair.has("private"));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }

    // Test the written status information.
    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_CONFIG_FILE_NO_KEY));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject config = new JSONObject(inLine);

      assertTrue(config.has(TestParameters.CLIENT_STATUS_PARAMS[3]));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }

  /**
   * Run the void generateSigningKey() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateSigningKey_1() throws Exception {
    Client fixture = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    assertFalse(fixture.isInitialised());
    assertEquals(1, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[1], fixture.getNextTask());

    ProgressListener listener = new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        assertTrue(value >= ClientTest.this.progress);
        ClientTest.this.progress = value;
      }
    };

    fixture.addProgressListener(listener);
    this.progress = 0;

    fixture.generateSigningKey();

    fixture.removeProgressListener(listener);

    assertFalse(fixture.isInitialised());
    assertEquals(2, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[2], fixture.getNextTask());

    assertTrue(new File(TestParameters.OUTPUT_CLIENT_SIGNING_KEY_FILE).exists());
    //CJC we no longer generate CSRs
    //assertTrue(new File(TestParameters.OUTPUT_CLIENT_CERT_SIGNING_FILE).exists());

    // SigningKey.json: single entry file.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_SIGNING_KEY_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject signingKey = new JSONObject(inLine);
      assertTrue(signingKey.has("clientSigningKey"));
      JSONObject clientKey =signingKey.getJSONObject("clientSigningKey");
      
      assertTrue(clientKey.has("pubKeyEntry"));
      assertTrue(clientKey.has("privateKey"));
      
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }

    // Test the written status information.
    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject config = new JSONObject(inLine);

      assertTrue(config.has(TestParameters.CLIENT_STATUS_PARAMS[0]));
      assertTrue(config.has(TestParameters.CLIENT_STATUS_PARAMS[1]));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }

  /**
   * Run the void importCertificate(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testImportCertificate_1() throws Exception {
    Client fixture = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE_NO_KEY);
    fixture.generateSigningKey();

    assertFalse(fixture.isInitialised());
    assertEquals(2, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[2], fixture.getNextTask());

    fixture.importSSLCertificate(TestParameters.CERTIFICATE_FILE);

    assertFalse(fixture.isInitialised());
    assertEquals(3, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[3], fixture.getNextTask());

    // Test the written status information.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_CONFIG_FILE_NO_KEY));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject config = new JSONObject(inLine);

      assertTrue(config.has(TestParameters.CLIENT_STATUS_PARAMS[2]));
      assertEquals(TestParameters.CERTIFICATE_FILE, config.get(TestParameters.CLIENT_STATUS_PARAMS[2]));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }

  /**
   * Run the void importSSLCertificate(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testImportSSLCertificate_1() throws Exception {
    Client fixture = new Client(TestParameters.OUTPUT_CLIENT_CONFIG_FILE_NO_KEY);
    fixture.generateSigningKey();

    assertFalse(fixture.isInitialised());
    assertEquals(2, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[2], fixture.getNextTask());

    fixture.importSSLCertificate(TestParameters.SSL_CERTIFICATE_FILE);

    assertFalse(fixture.isInitialised());
    assertEquals(3, fixture.getStatus());
    assertEquals(TestParameters.CLIENT_STATUS_PARAMS[3], fixture.getNextTask());

    // Test the written status information.
    BufferedReader inReader = null;

    try {
      inReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_CLIENT_CONFIG_FILE_NO_KEY));

      // Single object.
      String inLine = inReader.readLine();
      JSONObject config = new JSONObject(inLine);

      assertTrue(config.has(TestParameters.CLIENT_STATUS_PARAMS[2]));
      assertEquals(TestParameters.SSL_CERTIFICATE_FILE, config.get(TestParameters.CLIENT_STATUS_PARAMS[2]));
    }
    // Pass all exceptions up.
    finally {
      if (inReader != null) {
        inReader.close();
      }
    }
  }
}