/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ui;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.ballotgen.exceptions.ClientException;
import uk.ac.surrey.cs.tvs.ballotgen.init.CandidateIDs;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>ClientUITest</code> contains tests for the class <code>{@link ClientUI}</code>.
 */
public class ClientUITest {

  /**
   * Dummy web socket class. Does nothing.
   */
  private class DummyWebSocket extends WebSocket {

    @Override
    public void close(int arg0) {
    }

    @Override
    public void close(int arg0, String arg1) {
    }

    @Override
    protected void close(InvalidDataException arg0) {
    }

    @Override
    public Draft getDraft() {
      return null;
    }

    @Override
    public InetSocketAddress getLocalSocketAddress() {
      return null;
    }

    @Override
    public int getReadyState() {
      return 0;
    }

    @Override
    public InetSocketAddress getRemoteSocketAddress() {
      return null;
    }

    @Override
    public boolean hasBufferedData() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return false;
    }

    @Override
    public boolean isClosing() {
      return false;
    }

    @Override
    public boolean isConnecting() {
      return false;
    }

    @Override
    public boolean isOpen() {
      return false;
    }

    @Override
    public void send(byte[] arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(ByteBuffer arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(String arg0) throws NotYetConnectedException {
    }

    @Override
    public void sendFrame(Framedata arg0) {
    }

    @Override
    public void startHandshake(ClientHandshakeBuilder arg0) throws InvalidHandshakeException {
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Initialise the security provider.
    TestParameters.getInstance();

    // Copy over a clean version of the configuration file.
    TestParameters.copyFile(new File(TestParameters.CLEAN_CLIENT_CONFIG_FILE), new File(TestParameters.OUTPUT_CLIENT_CONFIG_FILE));

    // Generate candidate ids.
    CandidateIDs.create(TestParameters.CANDIDATES, TestParameters.PUBLIC_KEY_FILE, TestParameters.OUTPUT_PLAIN_TEXT_FILE,
        TestParameters.OUTPUT_ENCRYPTED_FILE);
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();

    // Put back the correct key files.
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_SIGNING_KEY_FILE), new File(
        TestParameters.OUTPUT_CLIENT_SIGNING_KEY_FILE));
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_PUBLIC_KEY_FILE), new File(
        TestParameters.OUTPUT_CLIENT_PUBLIC_KEY_FILE));
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_KEY_PAIR_FILE), new File(
        TestParameters.OUTPUT_CLIENT_KEY_PAIR_FILE));
    TestParameters.copyFile(new File(TestParameters.SAVED_KEYS, TestParameters.OUTPUT_CLIENT_CERT_SIGNING_FILE), new File(
        TestParameters.OUTPUT_CLIENT_CERT_SIGNING_FILE));
  }

  /**
   * Run the ClientUI(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = ClientException.class)
  public void testClientUI_1() throws Exception {
    ClientUI result = new ClientUI("rubbish");
    assertNull(result);
  }

  /**
   * Run the ClientUI(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testClientUI_2() throws Exception {
    ClientUI result = new ClientUI(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    assertNotNull(result);

    assertTrue(new File(TestParameters.UI_FOLDER, TestParameters.LOCALPORT_FILE).exists());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    String[] args = new String[0];

    ClientUI.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    // Modify the configuration to use a unique port.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    config.put(ConfigFiles.ClientConfig.UI_PORT, TestParameters.CLIENT_PORTS[0]);
    IOUtils.writeJSONToFile(config, TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    String[] args = new String[] { TestParameters.OUTPUT_CLIENT_CONFIG_FILE };

    ClientUI.main(args);
  }

  /**
   * Run the void processMessage(JSONObject,WebSocket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessMessage_1() throws Exception {
    // Modify the configuration to point to a different certificate folder and use a unique port.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    config.put(ConfigFiles.ClientConfig.CERTS_LOCATION, TestParameters.NEW_CERTS_FOLDER);
    config.put(ConfigFiles.ClientConfig.UI_PORT, TestParameters.CLIENT_PORTS[1]);
    IOUtils.writeJSONToFile(config, TestParameters.OUTPUT_CLIENT_CONFIG_FILE);

    ClientUI fixture = new ClientUI(TestParameters.OUTPUT_CLIENT_CONFIG_FILE);
    assertNotNull(fixture);

    WebSocket ws = new DummyWebSocket();
    JSONObject message = new JSONObject();

    message.put(MessageFields.TYPE, ClientConstants.UI.UI_TYPE_GET_STATUS);
    fixture.processMessage(message, ws);

    message.put(MessageFields.TYPE, ClientConstants.UI.UI_TYPE_GET_TYPE);
    fixture.processMessage(message, ws);

    message.put(MessageFields.TYPE, ClientConstants.UI.UI_TYPE_GEN_SIGNING_KEY);
    fixture.processMessage(message, ws);

    message.put(MessageFields.TYPE, ClientConstants.UI.UI_TYPE_GET_CSR);
    fixture.processMessage(message, ws);

    message.put(MessageFields.TYPE, ClientConstants.UI.UI_TYPE_IMPORT_CERTS);
    message.put(ClientConstants.UI.UI_REQ_CERT_STRING, TestParameters.CERTIFICATE_FILE);
    message.put(ClientConstants.UI.UI_REQ_SSLCERT_STRING, TestParameters.SSL_CERTIFICATE_FILE);
    fixture.processMessage(message, ws);

    message.put(MessageFields.TYPE, ClientConstants.UI.UI_TYPE_GEN_CRYPTO_KEY);
    fixture.processMessage(message, ws);

    message.put(MessageFields.TYPE, ClientConstants.UI.UI_TYPE_GEN_BALLOTS);
    fixture.processMessage(message, ws);
  }
}