/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.testsuite;

import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.KeyGeneration;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;


/**
 * TestSuite class for generating a cancel authorisation key to simulate central control
 * @author Chris Culnane
 *
 */
public class CancelKeyGen {

  /**
   * main method for running the CancelKeyGen functionality
   * 
   * File paths are hard-coded because this is for debug/testing only, not for general use
   * @param args String array of args
   * @throws CertificateRequestGenException 
   * @throws KeyGenerationException 
   * @throws KeyStoreException 
   * @throws IOException 
   * @throws CertificateException 
   * @throws NoSuchAlgorithmException 
   * @throws TVSSignatureException 
   * @throws CryptoIOException 
   * @throws CertificateStoringException 
   */
  public static void main(String[] args) throws CryptoIOException, KeyGenerationException, TVSSignatureException, CertificateStoringException, KeyStoreException, IOException {
    
      boolean genKey=false;
      boolean convertGen=true;
      CryptoUtils.initProvider();
      if(convertGen){
        TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
        tvsKeyStore.load("./Cancel/cancelKeyStore.bks","".toCharArray());
        BLSKeyPair bkp = BLSKeyPair.generateKeyPair();
        tvsKeyStore.addBLSKeyPair(bkp, "CancelAuth");
        tvsKeyStore.store("./Cancel/cancelKeyStore.bks");
        TVSKeyStore pubStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
        pubStore.addBLSPublicKey(bkp.getPublicKey(), "CancelAuth");
        pubStore.store("./Cancel/publicCert.bks");
        
        return;
      }
      if(genKey){
        KeyGeneration kg = new KeyGeneration("CancelAuth");
        kg.generateKeyAndCSR("./Cancel/", "./Cancel/cancelKeyStore.jks", SignatureType.DEFAULT);
        
      }else{
        KeyGeneration.importCertificates("./Cancel/CancelAuth.cer", "./Cancel/PeerCA.pem", "CancelAuth", "./Cancel/cancelKeyStore.jks", "");
      }
      
      
      
  }

}
