/*******************************************************************************
 * Copyright (c) 2014 2015 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.testsuite;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.VVoteWebServer;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;

/**
 * This purely for testing. It provides a way of simulating a central authorisation signature for the cancellation request.
 * 
 * @author Chris Culnane
 * 
 */
public class CancelProxyServer implements Runnable {

  /**
   * Simple web server to host the servlet
   */
  private VVoteWebServer              sws;
  
  /**
   * Map of servlets
   */
  private HashMap<String, NanoServlet> servlets = new HashMap<String, NanoServlet>();

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CancelProxyServer.class);
  
  /**
   * Construct a CancelProxyServlet that uses the key in the keyFilePath store
   * @param keyFilePath Key Store holding the cancel authorisation key
   */
  public CancelProxyServer(String keyFilePath) {
    CancelProxyServlet cancelProxy = new CancelProxyServlet(keyFilePath);
    servlets.put("CancelProxy", cancelProxy);
    logger.info("Created CancelProxyServer");
  }

  /**
   * Start the server on the port specified with the server root at the location specified
   * @param port int of the port to listen to
   * @param root File root directory to serve
   * @throws IOException 
   */
  public void startServer(int port, File root) throws IOException {
    sws = new VVoteWebServer("localhost", port, root, servlets);
    sws.start();
    logger.info("Started CancelProxyServer");
  }

  /**
   * Starts a CancelProxyServer by creating a servlet and starting the server of 8010
   * @param args 
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    CryptoUtils.initProvider();
    CancelProxyServer proxy = new CancelProxyServer("./Cancel/cancelKeyStore.bks");
    proxy.startServer(8010, new File("./Cancel/wwwroot/"));
    System.in.read();
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      sws.start();
    }
    catch (IOException e) {
      logger.error("Exception when starting CancelProxyServer",e);
    }

  }

}
