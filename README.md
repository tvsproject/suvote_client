vVoteClient
===========
The vVoteClient app is a proxy for communicating with the distributed Private WBB. It also performs setup actions, like ballot generation. There is a basic UI for performing the setup, after which it is run as a set of proxy servers. The code combines all the different types of client into one package, however, when deployed it is highly likely only a single type of client will operate on any single device. 

The client also contains a test suite webpage for sending sample messages of different types to the Private WBB. In order for the test suite to fully work all proxy servers must be started (POD, EVM and Cancel). There is an additional demo proxy for generating cancellation authorisation signatures - this should be removed before final deployment.

Issues
======
The code is written in Java 1.6 to allow it to be cross-compiled into Android. There are currently some naming conflicts with some of the third-party libraries (Bouncy Castle, Apache Commons). These can be resolved via jarjar which can be run on the jar to modify the package names to avoid the conflict. 

